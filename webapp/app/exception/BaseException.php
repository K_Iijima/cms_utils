<?php
abstract class BaseException extends Exception
{
    public function __construct($argMessage)
    {
        parent::__construct($argMessage, 0);
    }
}
