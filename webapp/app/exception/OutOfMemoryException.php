<?php
/**
 * メモリ系の処理に失敗した場合にThrowされるException
 */
class OutOfMemoryException extends BaseException
{
	/**
	 * コンストラクタ
	 * @param [type] $msg         [description]
	 */
    public function __construct($msg)
    {
        parent::__construct($msg);
    }
}
