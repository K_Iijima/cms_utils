<?php


/**
 * 画像ファイルに関する処理で使用する定数を提供するクラス
 */
class ImageFileConfig
{

	/**
	 * 作品画像のタイプ
	 * 追加する場合は以下に追記していく
	 * １度のファイルアップロードで複数の画像を生成する場合はそのタイプごとにDefineすること
	 * TODO ココにDefineするの微妙だなぁ。。
	 */
 	const IMG_TYPE_ORIGINAL 	= "IMG_TYPE_ORIGINAL";	// Image
	const IMG_TYPE_APPLE_A		= "IMG_TYPE_APPLE_A";	// キーUS2：3/1400x2100
	const IMG_TYPE_APPLE_B		= "IMG_TYPE_APPLE_B";	// キーUS1：1/1400x1400
	const IMG_TYPE_TITLE 		= "IMG_TYPE_TITLE";	// キーa480
	const IMG_TYPE_THUMBNAIL_1	= "IMG_TYPE_THUMBNAIL_1";	// サムネイルb32/32x32
	const IMG_TYPE_THUMBNAIL_2	= "IMG_TYPE_THUMBNAIL_2";	// ボックスe64/64x48
	const IMG_TYPE_THUMBNAIL_3	= "IMG_TYPE_THUMBNAIL_3";	// サムネイルf120/120x120
	const IMG_TYPE_BANNER		= "IMG_TYPE_BANNER";	// フィーチャーフォン/スマホ用バナー/240x48と480x96
	const IMG_TYPE_D_MENU		= "IMG_TYPE_D_MENU";	// ボックスg120/dメニュー動画検索
	const IMG_TYPE_PC_TITLE 	= "IMG_TYPE_PC_TITLE";	// PC用作品タイトル(m4:3)
	const IMG_TYPE_PC_TITLE_HD	= "IMG_TYPE_PC_TITLE_HD";	// 本店AndroidTV/CocoroVideo面だし(2Kディスプレイ用)
	const IMG_TYPE_PC_TITLE_4K	= "IMG_TYPE_PC_TITLE_4K";	// 本店AndroidTV/CocoroVideo面だし(4Kディスプレイ用)
	const IMG_TYPE_DIGITAL_SCREEN = "IMG_TYPE_DIGITAL_SCREEN";	// DS用タイトル画像/576x816

	/**
	 * 作品画像の縦横サイズ
	 * TODO 使用時に最新の要件を確認して値を設定すること
	 */
	const IMG_TYPE_PC_TITLE_X 	= 1920;
	const IMG_TYPE_PC_TITLE_Y 	= 1440;
	const IMG_TYPE_PC_TITLE_HD_X	= 544;
	const IMG_TYPE_PC_TITLE_HD_Y	= 408;
	const IMG_TYPE_PC_TITLE_4K_X	= 1080;
	const IMG_TYPE_PC_TITLE_4K_Y	= 810;
	const IMG_TYPE_APPLE_A_X		= 0;
	const IMG_TYPE_APPLE_A_Y		= 0;
	const IMG_TYPE_APPLE_B_X		= 0;
	const IMG_TYPE_APPLE_B_Y		= 0;
	const IMG_TYPE_TITLE_X 		= 0;
	const IMG_TYPE_TITLE_Y 		= 0;
	const IMG_TYPE_THUMBNAIL_1_X	= 0;
	const IMG_TYPE_THUMBNAIL_1_Y	= 0;
	const IMG_TYPE_THUMBNAIL_2_X	= 0;
	const IMG_TYPE_THUMBNAIL_2_Y	= 0;
	const IMG_TYPE_THUMBNAIL_3_X	= 0;
	const IMG_TYPE_THUMBNAIL_3_Y	= 0;
	const IMG_TYPE_BANNER_X	= 0;
	const IMG_TYPE_BANNER_Y	= 0;
	const IMG_TYPE_D_MENU_X	= 0;
	const IMG_TYPE_D_MENU_Y	= 0;
	const IMG_TYPE_DIGITAL_SCREEN_X = 0;
	const IMG_TYPE_DIGITAL_SCREEN_Y = 0;

	/*
	 * 作品画像の作成品質
 	 * TODO 使用時に最新の要件を確認して値を設定すること
	 */
	const IMG_TYPE_PC_TITLE_QUALITY 	= 87;	// PC用作品タイトル(m4:3)
	const IMG_TYPE_PC_TITLE_HD_QUALITY	= 87;	// 本店AndroidTV/CocoroVideo面だし(2Kディスプレイ用)
	const IMG_TYPE_PC_TITLE_4K_QUALITY	= 87;	// 本店AndroidTV/CocoroVideo面だし(4Kディスプレイ用)

	/************
	 * 画像ファイルをイメージサーバーへアップロードする際に使用するプロパティ値
	 ************/
    /* 画像送信先のディレクトリを作成する際に指定するパーミッション*/
    const DEFAULT_IMG_DIR_MODE = 0775;
    /* 画像ファイルを送信する際に指定するパーミッション*/
    const DEFAULT_IMG_FILE_MODE = 0666;

    /* アップロード先ディレクトリパスのうち、画像タイプごとに異なる部分 */
 	const IMG_TYPE_ORIGINAL_PATH 	= "";	// Image
	const IMG_TYPE_APPLE_A_PATH		= "";	// キーUS2：3/1400x2100
	const IMG_TYPE_APPLE_B_PATH		= "";	// キーUS1：1/1400x1400
	const IMG_TYPE_TITLE_PATH 		= "";	// キーa480
	const IMG_TYPE_THUMBNAIL_1_PATH	= "";	// サムネイルb32/32x32
	const IMG_TYPE_THUMBNAIL_2_PATH	= "";	// ボックスe64/64x48
	const IMG_TYPE_THUMBNAIL_3_PATH	= "";	// サムネイルf120/120x120
	const IMG_TYPE_BANNER_PATH		= "";	// フィーチャーフォン/スマホ用バナー/240x48と480x96
	const IMG_TYPE_D_MENU_PATH		= "";	// ボックスg120/dメニュー動画検索
	const IMG_TYPE_PC_TITLE_PATH 	= "/vm_pc/aspect4x3/";	// PC用作品タイトル(m4:3)
	const IMG_TYPE_PC_TITLE_HD_PATH	= "/vm_pc/aspect4x3_hd/";	// 本店AndroidTV/CocoroVideo面だし(2Kディスプレイ用)
	const IMG_TYPE_PC_TITLE_4K_PATH	= "/vm_pc/aspect4x3_4k/";	// 本店AndroidTV/CocoroVideo面だし(4Kディスプレイ用)
	const IMG_TYPE_DIGITAL_SCREEN_PATH = "";	// DS用タイトル画像/576x816
}