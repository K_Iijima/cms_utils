<?php

/**
 * 画像ファイルリサイズバッチ
 */
include '../model/ProgramFacade.php';
include '../model/ImageFileTitle.php';
include '../exception/BaseException.php';
include "../exception/IOException.php";
include "../exception/OutOfMemoryException.php";
require "../start/MetaCmsConst.php";

$usage = "usage : php resizeImage.php {画像格納ディレクトリ} {出力先ディレクトリ} {画像タイプ}";
$usage2 = "画像格納ディレクトリ配下の構造は {画像格納ディレクトリ}/版元ID/XXXX.jpeg である必要があります";

// 処理開始
$start = microtime(true);

printf("Start Exec \n");

$_arcCount = $argc - 1;

if($_arcCount != 3)
{
	printf("引数の数が足りないか、多すぎます Count: ". $_arcCount . " \n");
	printf($usage ."\n");
	exit(1);
}

$_argCheckResult = true;
if (!file_exists($argv[1]))
{
	$_argCheckResult = false;
	printf("第１引数で指定されたディレクトリが確認できません : " . $argv[1] ."\n");
}

if (!file_exists($argv[2]))
{
	$_argCheckResult = false;
	printf("第２引数で指定されたディレクトリが確認できません : " . $argv[2] ."\n");
}

// イメージサーバのパスを取得する関数を使ってチェック
if (ProgramFacade::getDstImagePathPrefix($argv[3]) === "")
{
	$_argCheckResult = false;
	printf("第３引数で指定された画像タイプは存在しません : " . $argv[3] ."\n");
}

if(!$_argCheckResult)
{
	printf("エラーが発生しました。 \n");
	printf($usage ."\n");
	exit(1);
}

printf("InputDir Path = " . $argv[1] ."\n");
printf("OutputDir Path = " . $argv[2] ."\n");
printf("Image Type = " . $argv[3] ."\n");

$_srcDir = $argv[1];
$_dstDir = $argv[2];
$_imageType = $argv[3];

$_successTitleList = array();
$_successPathList = array();
$_failedTitleList = array();

// 元画像の格納ディレクトリ配下のディレクトリを取得
$_publisher_dirs = scandir($_srcDir);

foreach ($_publisher_dirs AS $_pub_dir)
{
	if (strcmp($_pub_dir, ".") === 0 || strcmp($_pub_dir, "..") === 0 )
	{
		//scandir の結果には.(カレント) と ..(親)が含まれる
		//この場合は何もしない
		continue;
	}

	$_dstPubDir = $_dstDir . "/" . $_pub_dir;

	// 出力先のディレクトリが存在しなければ作成
	if (!file_exists($_dstPubDir))
	{
		$_ret = mkdir($_dstPubDir);

		if($_ret === false)
		{
			// ディレクトリ作成に失敗のは環境要因の可能性が高いのでエラー終了
			printf("出力先ディレクトリの作成に失敗 path:" . $_dstPubDir . "\n");
			exit(1);
		}
	}

	$_srcPubDir = $_srcDir . "/" . $_pub_dir;
	$_img_files = scandir($_srcPubDir);

	foreach ($_img_files AS $_srcImgfile)
	{
		if (strcmp($_srcImgfile, ".") === 0 || strcmp($_srcImgfile, "..") === 0 )
		{
			//scandir の結果には.(カレント) と ..(親)が含まれる
			//この場合は何もしない
			continue;
		}

		$_srcImgFilePath = $_srcPubDir . "/" . $_srcImgfile;

		// 元ファイルのフルパスからファイル名を取得し、さらに先頭１文字を切り取る
		$_titleID = substr(pathinfo($_srcImgfile, PATHINFO_FILENAME), 1);

		// ファイル名を作成
		// ファイル名を作りなおしているのは、異なる画像タイプへの変換をサポートするため
		$_fileName = ProgramFacade::createTitleImageFileName($_imageType, $_titleID);

		try
		{
			$_imgObj = new ImageFileTitle($_srcImgFilePath);
			$_imgObj->create($_imageType, JPEG_MIME_1, $_dstPubDir, $_fileName);
			$_successTitleList[] = $_titleID;
			$_successPathList[] = $_imgObj->getFileFullPath();
		}
		catch (Exception $e)
		{	// 出力に失敗しても処理を継続
			$_failedTitleList[] =  $_titleID;
		}
	}
}
echo "\n";
echo "Summary  ------------------\n";
echo "Success : ". count($_successTitleList) . "\n";
echo "Failed  : ". count($_failedTitleList) . "\n";

$end = microtime(true);
echo "処理時間：" . ($end - $start) . "秒\n";
echo "\n";

if (count($_successTitleList) > 0) {
    echo "Success Title ------------------\n";
    foreach ($_successTitleList as $_str) {
        echo $_str . "\n";
    }
}

echo "\n";

if (count($_successPathList) > 0) {
    echo "Success Path ------------------\n";
    foreach ($_successPathList as $_str) {
        echo $_str . "\n";
    }
}

echo "\n";

if (count($_failedTitleList) > 0) {
    echo "Failed Title ------------------\n";
    foreach ($_failedTitleList as $_str) {
        echo $_str . "\n";
    }
}

echo "\n";

if(count($_successTitleList) === 0 )
{
	printf($usage ."\n");
	printf($usage2 ."\n");
}





?>
