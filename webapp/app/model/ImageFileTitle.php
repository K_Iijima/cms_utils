<?php
require_once 'ImageFile.php';
require_once '../config/ImageFileConfig.php';

/**
 * 作品画像に関する操作を扱うクラス
 * 2018/04 m4:3画像およびそのバリエーションのみ対応
 **/
class ImageFileTitle extends ImageFile
{
	/**
	/**
	 * コンストラクタ
	 * @param [type] $ImgFile [description]
	 */
    public function __construct($ImgFile)
    {
        parent::__construct($ImgFile);
    }

	/**
	 * コンストラクタで指定した$_ImgFileをもとに、$_titleImageTypeの用途向け画像を作成する
	 * $_titleImageTypeに合わせて元ファイルをリサイズするが、処理後の画質は保証されない。
	 * 第１引数にIMG_TYPE_PC_TITLEを指定した場合、第2引数以降は無視される。 
	 * 作成したファイルはgetFilepath()で取得できる。
	 * $_ImgFileと$_titleImageTypeの用途のサイズが同じ場合、getFilepath()の返り値は元ファイルのパスとなる。
	 * @param [type] $titleImageType 作品画像のタイプ。
 	 * @param [type] $mime 作成するファイルのMIME(GIF_MIME/JPEG_MIME_1/JPEG_MIME_2/PNG_MIME)
	 * @param  string $dst_file_path ファイルの出力先。有効なパスを指定すること。
	 * @param  string $dst_file_name 出力するファイル名。ファイルの拡張子は$_mineの値に応じて自動的に設定する。
	 * @throws IOException ファイル出力に失敗
  	 * @throws OutOfMemoryException 画像データ生成処理でメモリ獲得に失敗
	 */
	public function create($titleImageType, $mime, $dst_file_path, $dst_file_name)
	{
		if ($titleImageType == ImageFileConfig::IMG_TYPE_PC_TITLE)
		{
			self::createPcTitleImg();
		}
		elseif ($titleImageType == ImageFileConfig::IMG_TYPE_PC_TITLE_HD)
		{
			self::createPcTitleImg_HD($mime, $dst_file_path, $dst_file_name);
		}
		elseif ($titleImageType == ImageFileConfig::IMG_TYPE_PC_TITLE_4K)
		{
			self::createPcTitleImg_4K($mime, $dst_file_path, $dst_file_name);
		}
		// 以降に画像タイプごとの分岐を追加していく
	}

	/******
	 * 以降には画像種別毎の処理を追加する
	 * 　例1. 画像が求めるサイズに満たない場合、不足域に余白を入れる
	 * 　例2. ピクセル数が基準に達していない場合は拡大する。
	 *******/

    /**
     * PC用作品タイトル(m4:3)を作成する
     * 2018/04時点では投入画像に対するバリデーションやリサイズを行っていないため、本関数では何もせずにパスだけ設定する
     * @return [type]                 [description]
     */
	private function createPcTitleImg()
	{	
		parent::setFileFullPath(parent::getImgFile());
	}

    /**
     * Cocoro Videoと本店AndroidTV用作品タイトル(フルHDディスプレイ向け)を作成する
     * @param  $mime 画像のMIME。出力時のフォーマットとなる。 
     * @param  $dst_file_path 出力先ファイルパス
　    * @param  $dst_file_name 出力先ファイル名
     */
	private function createPcTitleImg_HD($mime, $dst_file_path, $dst_file_name)
	{
		parent::createImageFile($mime, ImageFileConfig::IMG_TYPE_PC_TITLE_HD_X, 
			ImageFileConfig::IMG_TYPE_PC_TITLE_HD_Y, 
			ImageFileConfig::IMG_TYPE_PC_TITLE_HD_QUALITY, 
			$dst_file_path, $dst_file_name, 0, 0, 0, 0);
	}

    /**
     * Cocoro Videoと本店AndroidTV用作品タイトル(４Kディスプレイ向け)を作成する
     * @param  $mime 画像のMIME。出力時のフォーマットとなる。 
     * @param  $dst_file_path 出力先ファイルパス
     * @param  $dst_file_name 出力先ファイル名
     */
	private function createPcTitleImg_4K($mime, $dst_file_path, $dst_file_name)
	{
		parent::createImageFile($mime, ImageFileConfig::IMG_TYPE_PC_TITLE_4K_X, 
			ImageFileConfig::IMG_TYPE_PC_TITLE_4K_Y, 
			ImageFileConfig::IMG_TYPE_PC_TITLE_4K_QUALITY, 
			$dst_file_path, $dst_file_name, 0, 0, 0, 0);
	}
}
