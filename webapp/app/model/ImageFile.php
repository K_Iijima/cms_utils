<?php
require_once 'AbstractFile.php';

/**
 * 画像ファイルに関する操作を扱うクラス
 * 2018/04
 * 今後、以下のような処理を実装していく
 * 　・背景画像の生成(a480画像登録で、480x480未満の画像が投入された際に使用)
 * 　・画像同士の合成(a480画像登録で、投入画像と背景画像を合成)
 * TODO 本クラスはGIFやPNGにも対応できるようにコーディングされているが、2018/04時点ではテスト未実施。
 * GIFやPNGを扱う際は、ルートテストも実施すること
 **/
class ImageFile extends AbstractFile
{
	/**
	 * リサイズ時に指定するデフォルトの画質
	 * @var int
	 */
	//PHPのデフォルトは75だが、メタCMSのデフォルト値は実際の検証結果をもとに87を使用
	const DEFAULT_QUALITY = 87;

	/**
	 * 画像のタイプ
	 * TODO Enum値にする
	 * @var null
	 */
	private $imageType = null;

	/**
	 * アップロードされた画像ファイルのハンドル
	 * @var null
	 */
	private $imgFile = null;

    /**
     * コンストラクタ
     * TODO argImageTypeはEnum
     * @param [type] $_imageType イメージファイルのタイプ
     * @param  $_imgFile 画像ファイルのハンドル
     */
    protected function __construct($_imgFile)
    {
        $this->imgFile = $_imgFile;
    }

	/**
	 * 画像データをファイルに出力する
	 * コピー元とコピー先のXY座標を指定することで矩形領域指定も可能
	 * TODO 本関数の初回リリース時点では矩形領域指定についてテスト未実施(0固定)
	 * @param  string $mime       出力するファイルのMIME(GIF_MIME/JPEG_MIME_1/JPEG_MIME_2/PNG_MIME)
	 * @param  int $new_width     出力するファイルの横幅。元ファイルと異なるサイズが指定された場合、リサイズされる。
	 * @param  int $new_height    出力するファイルの高さ。元ファイルと異なるサイズが指定された場合、リサイズされる。
	 * @param  int $quality       出力時の画質(1-100) 0を指定した場合、DEFAULT_QUALITYで生成する。$mimeにGIF_MIMEを指定した場合は無効。
	 * @param  string $dst_file_path ファイルの出力先。有効なパスを指定すること。
	 * @param  string $dst_file_name 出力するファイル名。ファイルの拡張子は$_mineの値に応じて自動的に設定する。
	 * @param  int $src_x コピー元のx座標　元画像との整合が取れているかはチェックしないため、呼び出し元で保証すること
	 * @param  int $src_y コピー元のy座標 元画像との整合が取れているかはチェックしないため、呼び出し元で保証すること
 	 * @param  int $dst_x コピー先のx座標 作成する画像ファイルとの整合が取れているかはチェックしないため、呼び出し元で保証すること
	 * @param  int $dst_y コピー先のy座標　作成する画像ファイルとの整合が取れているかはチェックしないため、呼び出し元で保証すること
 	 * @throws IOException ファイル出力に失敗
 	 * @throws OutOfMemoryException 画像データ生成処理でメモリ獲得に失敗
	 */
	protected function createImageFile($mime, $new_width, $new_height, $quality, $dst_file_path, $dst_file_name, $src_x, $src_y, $dst_x, $dst_y)
	{
		// コンストラクタで指定したファイルを画像データとして扱うために変換
		$_base_img = self::getBaseImage();
		
		// アップロードされた画像のサイズを獲得する
		// 画像の幅
		$_base_width = self::getImageWidth($_base_img);
		
		// 画像の高さ
		$_base_height = self::getImageHeight($_base_img);
		
		// リサイズ後の画像データを展開するためのメモリ領域を確保する(リサイズ後のサイズを指定)
		$_outputMemArea = self::getMemoryArea($new_width, $new_height);
		
		// 画像データを作成する。
		self::createImageData($_outputMemArea, $_base_img, $dst_x, $dst_y, $src_x, $src_y, $new_width, $new_height, $_base_width, $_base_height);

		$_dst_file_fullpath = self::createImageFilePath($mime, $dst_file_path, $dst_file_name);

		// 作成した画像データをファイルに出力する
		self::outputImageFile($mime, $_outputMemArea, $_dst_file_fullpath, $quality);
	
		// 確保したメモリ領域を解放する
		self::freeMemoryArea($_outputMemArea);

		parent::setFileFullPath($_dst_file_fullpath);
	}

	/**
	 * 画像データをメモリに展開する
	 */
	private function createImageData(&$outputMemArea, $base_img, $dst_x, $dst_y, $src_x, $src_y, $new_width, $new_height, $base_width, $base_height)
	{
		$ret = ImageCopyResampled($outputMemArea, $base_img, $dst_x, $dst_y, $src_x, $src_y, $new_width, $new_height, $base_width, $base_height);

		if ( $ret === false )
		{
			throw new IOException("画像データの生成に失敗しました　base:%s " , $this->imgFile );
		}
	}

	/**
	 * ベースとなる画像をデータ化する
	 * @return resource 指定されたMineのresource
	 */
	private function getBaseImage()
	{
		$retBaseImage;

		// ファイルの拡張子を取得
		$image_ext = pathinfo($this->imgFile, PATHINFO_EXTENSION);

		// 拡張子を小文字に変換
		$image_ext = mb_strtolower($image_ext);

        if (strcmp($image_ext, "jpg") == 0 || strcmp($image_ext, "jpeg") == 0)
        {
            $retBaseImage = ImageCreateFromJPEG($this->imgFile);
        }
        elseif (strcmp($image_ext, "gif") == 0)
        {
            $retBaseImage = ImageCreateFromGIF($this->imgFile);
        }
        elseif (strcmp($image_ext, "png") == 0)
        {
            $retBaseImage = ImageCreateFromPNG($this->imgFile);
        }
        else 
        {
        	$retBaseImage = ImageCreateFromJPEG($this->imgFile);
        }

        return $retBaseImage;
	}

	/**
	 * 画像の横幅を取得する
	 * @param  resource $img_resource 画像リソース
	 * @return int 画像の横幅
	 */
	private function getImageWidth($img_resource)
	{
		return ImageSx($img_resource);
	}

	/**
	 * 画像の高さを取得する
	 * @param  resource $img_resource 画像リソース
	 * @return int 画像の高さ
	 */
	private function getImageHeight($img_resource)
	{
		return ImageSy($img_resource);
	}

	/**
	 * 画像データを展開するためのメモリ領域を獲得する
	 * @param  int $_arg_width  画像データの横幅
	 * @param  int $_arg_height 画像データの高さ
	 * @return resource 指定された大きさの画像を表すメモリ領域
	 */
	private function getMemoryArea($arg_width, $arg_height)
	{
		$_ret = ImageCreateTrueColor($arg_width, $arg_height);

		if ($_ret === false) 
		{
			$_msg = sprintf("画像データを展開するために必要なメモリが獲得できませんでした width:%s height:%s", $arg_width, $arg_height);

			throw new OutOfMemoryException($_msg);
		}

		return $_ret;
	}

	/**
	 * [createImageFilePath description]
	 * @param  [type] $mime          [description]
	 * @param  [type] $dst_file_path [description]
	 * @param  [type] $dst_file_name [description]
	 * @return [type]                 [description]
	 */
	protected function createImageFilePath($mime, $dst_file_path, $dst_file_name)
	{
		$_file_Ext = self::getImageExtFromMime($mime);

		if (mb_strcut($dst_file_path, -1))
		{
			$_imageFilePath = $dst_file_path . "/" . $dst_file_name . $_file_Ext;
		}else
		{
			$_imageFilePath = $dst_file_path . $dst_file_name . $_file_Ext;
		}
		return $_imageFilePath;
	}

	/**
	 * [getImageExt description]
	 * @param  [type] $mime [description]
	 * @return [type]        [description]
	 */
	private function getImageExtFromMime($mime)
	{
		//TODO ProgramFacadeに同様のロジックがあるが
		//循環参照になる可能性があるので本クラス内に作成した。
		$retExt;

        if ($mime == JPEG_MIME_1 || $mime == JPEG_MIME_2)
        {
            $retExt = ".jpg";
        }
        elseif ($mime == GIF_MIME)
        {
            $retExt = ".gif";
        }
        elseif ($mime == PNG_MIME)
        {
            $retExt = ".png";
        }
        else
        {	// 想定外の値が渡ってきた場合は一番汎用的なjpgに
        	$retExt = ".jpg";
        }
        return $retExt;
	}

	/**
	 * 指定されたmimeに応じたフォーマットで画像ファイルを出力する
	 * @param  string $mime       			出力するファイルのフォーマット(GIF_MIME/JPEG_MIME_1/JPEG_MIME_2/PNG_MIME)
	 * @param  resource $outputMemArea 	画像データが展開されているメモリ領域
	 * @param  string $dst_file_fullpath 	ファイルを出力するパス(フルパス)
	 * @param  int $_quality       			出力時の画質(1-100) 0を指定した場合、DEFAULT_QUARITYで生成する。$mimeにGIF_MIMEを指定した場合は無効。
	 */
	private function outputImageFile($mime, $outputMemArea, $dst_file_fullpath, $quality)
	{
		$_ret = false;

        if ($mime == JPEG_MIME_1 || $mime == JPEG_MIME_2)
        {
			if ($quality === 0 )
			{
				$quality = self::DEFAULT_QUALITY;
			}

            $_ret = ImageJPEG($outputMemArea, $dst_file_fullpath, $quality);
        }
        elseif ($mime == GIF_MIME)
        {
        	// GIFはQuality指定できない
            $_ret = ImageGIF($outputMemArea, $dst_file_fullpath);
        }
        elseif ($mime == PNG_MIME)
        {
        	$_png_quality = 0;
        	
        	if ($quality < 10)
        	{
        		$_png_quality = 9;
        	}
        	else
        	{
        		// PNGのQUALITYは0-9なので無理やり変換しておく
        		// TODO PNG出力機能を利用する際はPNG用のデフォルトを検討するなど実装をチェック
        		$_png_quality = abs(floor($quality/10) - 10);
        	}

            $_ret = ImagePNG($outputMemArea, $dst_file_fullpath, $_png_quality);
        }
        else
        {
			if ($quality === 0 )
			{
				$quality = self::DEFAULT_QUALITY;
			}

        	// 想定外の値が渡ってきた場合は一番汎用的なjpgに
            $_ret =ImageJPEG($outputMemArea, $dst_file_fullpath, $quality);
        }

        // エラー判定
        if ($_ret === false)
        {
        	$_msg = sprintf("画像ファイルの出力に失敗しました path:%s", $dst_file_fullpath);
			throw new IOException($_msg);
        }
	}

	/**
	 * 画像データ展開用に獲得したメモリ領域を解放する
	 * @param  resource $outputMemArea 画像データ展開用に獲得したメモリ領域
	 */
	private function freeMemoryArea($outputMemArea)
	{
		if(isset($outputMemArea))
		{
			ImageDestroy($outputMemArea);
		}
	}

	/**
	 * コンストラクタで指定されたFileを返す
	 */
	protected function getImgFile()
	{
		return $this->imgFile;
	}

}
