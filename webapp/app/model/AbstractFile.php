<?php


abstract class AbstractFile
{
	/* ファイルのフルパス */
	private $filePath = "";

	/**
	 * このクラスで扱うファイルのフルパスを設定する
	 * @param string $_arg_file_full_path ファイルのフルパス
	 */
	protected function setFileFullPath($_arg_file_full_path)
	{
		$this->filePath = $_arg_file_full_path;
	}

	/**
	 * このクラスで扱うファイルのフルパスを返す
	 */
	public function getFileFullPath()
	{
		return $this->filePath;
	}

	/**
	 * $filePathに有効なファイルパスが設定されている場合、ファイルを削除する
	 */
	public function removeFile()
	{
		if (File::exists($this->filePath))
		{
			unlink($this->filePath);
		}
	}
}
