<?php

// namespace MetaCmsFacades;
// use app\models\ImgProcess;
// require_once 'ImgProcess.php';

class ProgramFacade
{
    /**
     * 画像ファイルの配備サーバリスト
     * @var unknown
     */
    private static $imgDeployList = array(
        /*
        array(
            'host' => 'img1',
            'login' => 'qtvadm',
            'password' => 'tekopfbw',
            'path' => "/data/doc_root/service/img_video/webapps/image",
        ),
        array(
            'host' => 'img2',
            'login' => 'qtvadm',
            'password' => 'tekopfbw',
            'path' => "/data/doc_root/service/img_video/webapps/image",
        )*/
        
        array(
            'host' => 'stm22',
            'login' => 'qtvadm',
            'password' => 'tekopfbw',
            'path' => "/data/doc_root/service/dev_img_videomarket_jp/webapps/",
        )    
    );

    /**
     * キャスト画像の配備サーバリスト
     * @var unknown
     */
    private static $castImgDeployList = array(
        array(
            'host'=>	'img1',
            'login'=>	'qtvadm',
            'password'=>	'tekopfbw',
            'path'=>	'/data/doc_root/service/img_video/webapps/img3/',
        ),
        array(
            'host'=>	'img2',
            'login'=>	'qtvadm',
            'password'=>	'tekopfbw',
            'path'=>	'/data/doc_root/service/img_video/webapps/img3/',
        ),
        array(
            'host'=>	'img1',
            'login'=>	'qtvadm',
            'password'=>	'tekopfbw',
            'path'=>	'/data/doc_root/service/img_video/webapps/image/ktai/chara/',
        ),
        array(
            'host'=>	'img2',
            'login'=>	'qtvadm',
            'password'=>	'tekopfbw',
            'path'=>	'/data/doc_root/service/img_video/webapps/image/ktai/chara/',
        ),
    );

    /**
     * タイトル画像のURLを設定する.
     * @param unknown $info
     */
    public static function setTitleImgUrl(&$info)
    {

        // キー画像URLを設定
        if ($info->title_image_mime != "") {
            $info->img_url =
                IMG_BASE_URL . TITLE_IMG_URL_PC_BASE . $info->publisher_id . "/"
                    . "a" . $info->full_title_id . ".jpg";
        }
        else {
            $info->img_url = TITLE_NO_IMG_PATH;
        }
    }

    /**
     * 指定IDのタイトル基本情報を取得する
     * @param unknown $id
     */
    public static function getTitleBaseInfo($id)
    {
        // タイトル情報を取得
        $_titleInfo = Title::where('full_title_id', '=', $id)->first();

        // タイトル画像を設定する
        self::setTitleImgUrl($_titleInfo);

        // 属性情報を設定する
        self::setTitleAttributeInfo($_titleInfo);

        // 表示情報を設定する
        self::getTitleDisplayControlInfo($_titleInfo);

        // 前後のタイトル情報を設定する
        self::setTitleInfoBeforeAndAfter($_titleInfo);

        // UniversalSearchで2:3画像(映画用)出すか出さないかの判定
        self::getTitleInfoMovieOrNot($_titleInfo);

        return $_titleInfo;
    }

    /**
     * 指定タイトルの前後のタイトル情報を設定する.
     * @param unknown $info
     */
    public static function setTitleInfoBeforeAndAfter(&$info)
    {
        $info->prefixTitleUrl = "";
        $info->suffixTitleUrl = "";

        $_prefixTitleId = "";
        $_suffixTitleId = "";

        // 指定タイトルと同じ版元のタイトル情報を取得する
        $_titleList = Title::where('publisher_id', '=', $info->publisher_id)->get();
        $_tmpId = "";
        $_check = false;
        foreach ($_titleList as $_titleInfo) {
            if ($_tmpId != "") {
                // tmpIdが設定されている場合、prefixTitleIdに設定する
                $_prefixTitleId = $_tmpId;
            }

            if ($info->full_title_id == $_titleInfo->full_title_id) {
                // 表示しているタイトルとリストのタイトルが同じだった場合、checkをtrueにする
                $_check = true;
            }
            elseif ($_check === false) {
                // check===fales === 表示しているタイトルに達していないのでtmpIdにリストのIDを設定
                $_tmpId = $_titleInfo->full_title_id;
            }
            elseif ($_check === true) {
                // 表示しているタイトルの次のタイトルなのでsuffixTitleIdに設定
                $_suffixTitleId = $_titleInfo->full_title_id;
                break;
            }
        }

        if ($_prefixTitleId != "") {
            // 前の作品のURLを取得する
            $info->prefixTitleUrl = self::getUrl($_prefixTitleId);
        }

        if ($_suffixTitleId != "") {
            // 次の作品のURLを取得する
            $info->suffixTitleUrl = self::getUrl($_suffixTitleId);
        }
    }


    /**
     * UniversalSearchで2:3画像(映画用)出すか出さないかの判定.
     * @param unknown $info
     */
    public static function getTitleInfoMovieOrNot(&$info)
    {
        $info->isMovie = '0';

        // 指定タイトルと同じ版元のタイトル情報を取得する
        $_info = TitleGenreInfo::where('full_title_id', '=', $info->full_title_id)
                               ->where('large_category_id', '=', '2')
                               ->first();
        if ($_info != null) {

            $info->isMovie = '1';
        }
    }

    /**
     * 現在表示している画面の作品ID部分を変更したURLを取得する
     * @param unknown $titleId
     * @return number
     */
    public static function getUrl($titleId)
    {
        // 現在のURLを「/」で分割し取得する
        $_urls = explode("/" , $_SERVER['REQUEST_URI']);
        $_paramCnt = count($_urls);
        $_url = "";
        for($_i = 0; $_i < ($_paramCnt - 1); $_i++) {
            if ($_i != 0) {
                $_url .= "/";
            }

            if ($_urls[$_i] == "storyEdit") {
                $_urls[$_i] = "storyList";
            }

            $_url .= $_urls[$_i];
        }

        $_url .= "/" . $titleId;

        return $_url;
    }

    /**
     * 付属情報を取得する
     * @param unknown $info
     */
    public static function setTitleAttributeInfo(&$info)
    {
        // カテゴリ情報を取得する
        $_categoryData = CategoryName::all();

        $_categoryInfoList = array();
        foreach ($_categoryData as $_categoryInfo) {
            $_categoryInfoList[$_categoryInfo->id] = $_categoryInfo;
        }

        // サブジャンル情報を取得する
        $_subGenreData = SubGenreName::all();
        $_subGenreInfoList = array();
        foreach ($_subGenreData as $_subGenreInfo) {
            $_subGenreInfoList[$_subGenreInfo->id] = $_subGenreInfo;
        }

        // 国情報を取得する
        $_countryData = CountryName::all();
        $_countryInfoList = array();
        foreach ($_countryData as $_countryInfo) {
            $_countryInfoList[$_countryInfo->id] = $_countryInfo;
        }

        // シリーズ情報を取得する
        $_seriesData = Series::all();
        $_seriesList = array();
        foreach ($_seriesData as $_seriesInfo) {
            $_seriesList[$_seriesInfo->series_id] = $_seriesInfo;
        }

        // 指定タイトルのカテゴリ情報を取得する
        $_titleCategory = TitleGenre::where('publisher_id', '=', $info->publisher_id)
            ->where('title_id', '=', $info->title_id)->first();
        // 配列に変換
        if ($_titleCategory != "") {
            $_titleCategory = json_decode($_titleCategory, true);

            // カテゴリ名を設定
            if ($_titleCategory['category_id'] != "") {
                $info->category_name = $_categoryInfoList[$_titleCategory['category_id']]->category_name;
            }
            else {
                $info->category_name = "";
            }

            // 制作国を設定
            $_countryStr = "";
            foreach ($_titleCategory as $_key => $_val) {
                if (preg_match('/^country/', $_key) == 1) {
                    // countryで始まる場合
                    if ($_val != "" && $_val != "0") {
                        if ($_countryStr != "") {
                            $_countryStr .= ",";
                        }
                        $_countryStr .= $_countryInfoList[$_val]->country_name;
                    }
                }
            }
            $info->country_str = $_countryStr;

            // 制作年を設定
            $info->year_from = $_titleCategory['year_from'];

            // サブジャンルを設定
            $_subGenreStr = "";
            foreach ($_titleCategory as $_key => $_val) {
                if (preg_match('/^gid/', $_key) == 1) {
                    // gidで始まる場合
                    if ($_val != "" && $_val != "0") {
                        if ($_subGenreStr != "") {
                            $_subGenreStr .= ",";
                        }
                        $_subGenreStr .= $_subGenreInfoList[$_val]->subgenre_name;
                    }
                }
            }
            $info->subGenre_list = $_subGenreStr;
        }
        else {
            $info->category_name = "";
            $info->country_str = "";
            $info->year_from = "";
            $info->subGenre_list = "";
        }

        // 指定タイトルのシリーズ情報を取得する
        $_titleSeries = TitleSeries::where('publisher_id', '=', $info->publisher_id)
            ->where('title_id', '=', $info->title_id)->first();
        $_seriesStr = "";
        if ($_titleSeries != "") {
            // 配列に変換
            $_titleSeries = json_decode($_titleSeries, true);
            foreach ($_titleSeries as $_key => $_val) {
                if (preg_match('/^series_id/', $_key) == 1) {
                    // series_idで始まる場合
                    if ($_val != "" && $_val != "0") {
                        if ($_seriesStr != "") {
                            $_seriesStr .= ",";
                        }
                        $_seriesStr .= $_seriesList[$_val]->series_name;
                    }
                }
            }
        }
        $info->series_str = $_seriesStr;

        // 指定タイトルのフルアプリ用サイトフラグを取得
        // TV再生可否フラグも同時に取得・設定
        $_titleDelivery = TitleDelivery::find($info->full_title_id);
        $_vm_smt_appli = '0';
        $_vmCcFlag = '0';
        $_airPlayFlag = '0';
        $_vmPcSubs = '1';
        $_vm_androidtv = '0';
        $_vm_appletv = '0';
        $_mb_smt = '0';
        $_mb_pc = '0';
        $_mb_androidtv = '0';
        $_vm_fullhd = '0';
        $_pachidoluck_smt = '0';
        $_ccrv_tv = '0';
        $_minto_smt = '0';
        $_ccrv_smt = '0';
        $_ds_pc = '0';

        if (!empty($_titleDelivery)) {
            $_vm_smt_appli = $_titleDelivery->vm_smt_appli;
            $_vmCcFlag = $_titleDelivery->vm_cc;
            $_airPlayFlag = $_titleDelivery->airplay;
            $_vmPcSubs = $_titleDelivery->vm_pc_subs;
            $_vm_androidtv = $_titleDelivery->vm_androidtv;
            $_vm_appletv = $_titleDelivery->vm_appletv;
            $_mb_smt = $_titleDelivery->mb_smt;
            $_mb_pc = $_titleDelivery->mb_pc;
            $_mb_androidtv = $_titleDelivery->mb_androidtv;
            $_vm_fullhd = $_titleDelivery->vm_fullhd;
            $_pachidoluck_smt = $_titleDelivery->pachidoluck_smt;
            $_ccrv_tv = $_titleDelivery->ccrv_tv;
            $_minto_smt = $_titleDelivery->minto_smt;
            $_ccrv_smt = $_titleDelivery->ccrv_smt;
            $_ds_pc = $_titleDelivery->ds_pc;
        }

        $info->vm_smt_appli = $_vm_smt_appli;
        $info->vm_cc = $_vmCcFlag;
        $info->airplay = $_airPlayFlag;
        $info->vm_pc_subs = $_vmPcSubs;
        $info->vm_androidtv = $_vm_androidtv;
        $info->vm_appletv = $_vm_appletv;
        $info->mb_smt = $_mb_smt;
        $info->mb_pc = $_mb_pc;
        $info->mb_androidtv = $_mb_androidtv;
        $info->vm_fullhd = $_vm_fullhd;
        $info->pachidoluck_smt = $_pachidoluck_smt;
        $info->ccrv_tv = $_ccrv_tv;
        $info->minto_smt = $_minto_smt;
        $info->ccrv_smt = $_ccrv_smt;
        $info->ds_pc = $_ds_pc;
    }

    /**
     * 指定のmimeに対応した拡張子を返却する
     */
    public static function getExt($mime)
    {
        $_ext = "";
        if ($mime == GIF_MIME) {
            $_ext = ".gif";
        }
        elseif ($mime == JPEG_MIME_1) {
            $_ext = ".jpg";
        }
        elseif ($mime == JPEG_MIME_2) {
            $_ext = ".jpg";
        }
        elseif ($mime == PNG_MIME) {
            $_ext = ".png";
        }
        return $_ext;
    }

    /**
     * MIMEをファイル名から取得する
     * @param unknown $fileName
     * @return string|boolean
     */
    public static function getMIME($fileName) {
        if (preg_match('/\.gif$/i', $fileName)) {
            return GIF_MIME;
        }
        if (preg_match('/\.jpg$/i', $fileName)) {
            return JPEG_MIME_1;
        }
        if (preg_match('/\.jpeg$/i', $fileName)) {
            return JPEG_MIME_2;
        }
        return false;
    }

    /**
     * コピー禁止フラグを画像に埋め込む
     * @param unknown $filePath
     * @param unknown $fileType
     * @return boolean
     */
    public static function setCopyFlg($filePath, $fileType)
    {
        $co_txt = "kddi_copyright=on,copy=\"NO\"";

        $mime = $fileType;
        if(strcasecmp($fileType, "image/pjpeg") == 0) {
            $mime = "image/jpeg";
        }
        if($mime == "image/gif") {
            $ret = put_gif_Comment($filePath, $co_txt);
        } else {
            $headers = get_jpeg_header_data($filePath);
            $new_headers = put_jpeg_comment($headers, $co_txt);
            $ret = put_jpeg_header_data($filePath, $filePath, $new_headers);
            if($ret !== false) {
                $ret = true;
            }
        }
        return $ret;
    }

    /**
     * タイトルの表示設定情報を取得する
     * @param unknown $info
     */
    public static function getTitleDisplayControlInfo(&$info)
    {
        $_displayInfo = TitleDisplayControl::where('publisher_id', '=', $info->publisher_id)
            ->where('title_id', '=', $info->title_id)->first();

        if ($_displayInfo != "") {
            $_displayInfo = json_decode($_displayInfo, true);

            // 各情報をタイトル情報へ設定
            $info->freepack_pickup_flg = $_displayInfo['freepack_pickup_flg'];
            $info->titlepage_freepack_hide_flg = $_displayInfo['titlepage_freepack_hide_flg'];
            $info->titlepage_castinfo_hide_flg = $_displayInfo['titlepage_castinfo_hide_flg'];
            $info->cast_display_type = $_displayInfo['cast_display_type'];
            $info->titlepage_subgenre_hide_ids = $_displayInfo['titlepage_subgenre_hide_ids'];
            $info->packlist_paging_flg = $_displayInfo['packlist_paging_flg'];
            $info->titlepage_display_type_pc = $_displayInfo['titlepage_display_type_pc'];
        }
    }

    /**
     * 各話画像のアップロードを行う
     * @param unknown $fullStoryId
     * @param unknown $uploadImgFile
     */
    public static function uploadStoryImg($fullStoryId, $uploadImgFile)
    {

        $_fileName = "s" . $fullStoryId . "a.jpg";

            // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 画像サイズを取得する
            $_img = ImageCreateFromJPEG($uploadImgFile);
            $_width = ImageSx($_img);
            $_height = ImageSy($_img);

            //----- 640x480向け
            if ($_width == "640" && $_height == "480") {
                Log::debug(__METHOD__ . ':' . __LINE__);
                $_path = $_serverInfo['path'] . "/android/640x480/" . substr($fullStoryId, 0, 3) . "/";

                // ディレクトリを作成する
                ssh2_sftp_mkdir($_sftp, $_path, 0775);

                // ファイル転送
                if (!ssh2_scp_send($_con, $uploadImgFile, $_path . $_fileName, 0666)) {
                    ssh2_disconnect($_con);
                    return "ファイル転送に失敗しました";
                }
            }
            //----- 640x480向け

            //----- 480x360向け
            if ( ($_width == "640" && $_height == "480") || ($_width == "480" && $_height == "360")) {
                $_workImg = $uploadImgFile;
                Log::debug(__METHOD__ . ':' . __LINE__);
                if ($_width == "640" && $_height == "480") {
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(480, 360);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,480,360, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                }

                $_path = $_serverInfo['path'] . "/android/480x360/" . substr($fullStoryId, 0, 3) . "/";

                // ディレクトリを作成する
                ssh2_sftp_mkdir($_sftp, $_path, 0775);

                // ファイル転送
                if (!ssh2_scp_send($_con, $_workImg, $_path . $_fileName, 0666)) {
                    ssh2_disconnect($_con);
                    return "ファイル転送に失敗しました";
                }
            }
            //----- 480x360向け

            //----- 320x240向け
            if ( ($_width == "640" && $_height == "480") || ($_width == "480" && $_height == "360") || ($_width == "320" && $_height == "240")) {
                Log::debug(__METHOD__ . ':' . __LINE__);
                $_workImg = $uploadImgFile;
                if ($_width == "640" && $_height == "480") {
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(320, 240);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,320,240, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                }
                elseif ($_width == "480" && $_height == "360") {
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(320, 240);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,320,240, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                }

                $_path = $_serverInfo['path'] . "/android/320x240/" . substr($fullStoryId, 0, 3) . "/";

                // ディレクトリを作成する
                ssh2_sftp_mkdir($_sftp, $_path, 0775);

                // ファイル転送
                if (!ssh2_scp_send($_con, $_workImg, $_path . $_fileName, 0666)) {
                    ssh2_disconnect($_con);
                    return "ファイル転送に失敗しました";
                }
            }
            //----- 320x240向け

            //----- 240x180向け
            if ( ($_width == "640" && $_height == "480") || ($_width == "480" && $_height == "360") || ($_width == "320" && $_height == "240")) {
                Log::debug(__METHOD__ . ':' . __LINE__);
                $_workImg = "";
                if ($_width == "640" && $_height == "480") {
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(240, 180);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,240,180, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                }
                elseif ($_width == "480" && $_height == "360") {
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(240, 180);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,240,180, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                }
                else {
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(240, 180);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,240,180, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                }

                $_path = $_serverInfo['path'] . "/android/240x180/" . substr($fullStoryId, 0, 3) . "/";

                // ディレクトリを作成する
                ssh2_sftp_mkdir($_sftp, $_path, 0775);

                // ファイル転送
                if (!ssh2_scp_send($_con, $_workImg, $_path . $_fileName, 0666)) {
                    ssh2_disconnect($_con);
                    return "ファイル転送に失敗しました";
                }
            }
            //-----  240x180向け

            //----- 192x144向け
            if ( ($_width == "640" && $_height == "480") || ($_width == "480" && $_height == "360") || ($_width == "320" && $_height == "240")) {
                $_workImg = $uploadImgFile;
                Log::debug(__METHOD__ . ':' . __LINE__);
                if ($_width == "640" && $_height == "480") {
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(192, 144);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,192,144, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                }
                elseif ($_width == "480" && $_height == "360") {
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(192, 144);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,192,144, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                }
                else {
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(192, 144);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,192,144, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                }

                $_path = $_serverInfo['path'] . "/ktai/story/" . substr($fullStoryId, 0, 3) . "/";

                // ディレクトリを作成する
                ssh2_sftp_mkdir($_sftp, $_path, 0775);

                // ファイル転送
                if (!ssh2_scp_send($_con, $_workImg, $_path . $_fileName, 0666)) {
                    ssh2_disconnect($_con);
                    return "ファイル転送に失敗しました";
                }
            }
            //-----  192x144向けend
            // 4:3ではない画像の処理
            if ($_width != "640" || $_height != "480"){
                Log::debug(__METHOD__ . ':' . __LINE__);
            }
        }
    }

    /**
     * 各話画像の4:3をoriginalと同時に4:3の処理
     * @param unknown $fullStoryId
     * @param unknown $uploadImgFile
     */
    public static function uploadStoryWithOriginalImg($fullStoryId, $uploadImgFile)
    {
        $_fileName = "s" . $fullStoryId . "a.jpg";
            // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 画像サイズを取得する
            $_img = ImageCreateFromJPEG($uploadImgFile);
            $_width = ImageSx($_img);
            $_height = ImageSy($_img);

            //---縦長と正方形の場合---
            if( ($_width < $_height) || ($_width == $_height) ){

                    //拡大率を計算する
                    $per_h = 480 / $_height;
                    $per_w = 640 / $_width;

                    //値が大きい方で乗算する
                    if($per_h < $per_w){
                        //new_width,heightを作成する
                        $new_src_w = $_width * $per_w;
                        $new_src_h = $_height * $per_w;
                    }
                    elseif($per_w < $per_h){
                        $new_src_w = $_width * $per_h;
                        $new_src_h = $_height * $per_h;
                    }
                    elseif($per_w == $per_h){
                        $new_src_w = $_width * $per_h;
                        $new_src_h = $_height * $per_h;
                    }

                    //画像を拡大
                    $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                    $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                    imagecopyresampled( $uploadImgFile_created , $uploadImgFileJpeg , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $_width , $_height ); // 拡大された元画像
                    //パスを出す
                    $uploadImgFilePass = storage_path($uploadImgFile_created);
                    ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 90);

                    //ファイル名を統一
                    $uploadImgFile_large = $uploadImgFile_created;//拡大を確認

                    //-----余白入り画像作成　ここから
                    //画像ファイル名を指定
                    $_fileName = 0;
                    $_fileName = "s" . $fullStoryId . "a.jpg";

                    //width,heightを再計測する
                     $_width = imagesx($uploadImgFile_large);
                     $_height = imagesy($uploadImgFile_large);

                    // 保存する画像のサイズ
                    $dst_w = 480;
                    $dst_h = 360;

                    // 保存する画像に対する元画像の比率
                    $per_h   = $dst_h/$_height;
                   //元画像をリサイズ（width,height）
                    $new_src_w = $_width * $per_h;
                    $new_src_h = $_height * $per_h;

                    //中央点の計算
                    $centerX = $new_src_w/2;
                    $centerY = $new_src_h/2;

                    //開始点の算出
                    $startPointX = $dst_w/2 - $centerX;
                    $startPointY = $dst_w/2 - $centerY;

                    //画像を縮小
                    $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                    imagecopyresampled( $uploadImgFile_created , $uploadImgFile_large , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $_width , $_height ); // 縮小された元画像
                    //パスを出す
                    $uploadImgFilePass = storage_path($uploadImgFile_created);
                    ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 75);

                    // 背景画像を生成する
                    $dst = imagecreatetruecolor($dst_w, $dst_h);
                    $bg_color = imagecolorallocate( $dst, 255, 255, 255 );
                    imagefilledrectangle( $dst , 0 , 0 , $dst_w , $dst_h , $bg_color ); // 白の背景を$lengthのサイズで作成
                    //パスを出す
                    $bgPass = storage_path($dst);
                    ImageJPEG($dst, $bgPass, 75);

                    //合成する
                    imagecopy( $dst , $uploadImgFile_created , $startPointX  , 0 , 0 , 0  , $new_src_w , $new_src_h );

                    //---編集画像のPassを取得
                    $filePass = storage_path($dst);
                    //---画像容量を落とす(205kb以下)
                    ImageJPEG($dst, $filePass, 70);

                    // imageサーバーへ接続して保存する
                    // ディレクトリを設定
                    $_keyPath = $_serverInfo['path'] . "/android/480x360/" . substr($fullStoryId, 0, 3) . "/";

                    // ディレクトリを作成
                    ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);

                    // ファイルを転送
                    if (!ssh2_scp_send($_con, $filePass, $_keyPath . $_fileName, 0666)) {
                        return "480x360のファイル転送に失敗しました";
                    }

                    // 320*240 リサイズを行う
                    $_workImg = $filePass;
                    $_out = ImageCreateTrueColor(320, 240);
                    ImageCopyResampled($_out, $dst,
                        0,0,0,0, 320, 240, 480, 360);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                    $_320_keyPath = $_serverInfo['path'] . "/android/320x240/" . substr($fullStoryId, 0, 3) . "/";

                    // ディレクトリを作成する
                    ssh2_sftp_mkdir($_sftp, $_320_keyPath, 0775);

                    // ファイル転送
                    if (!ssh2_scp_send($_con, $_workImg, $_320_keyPath . $_fileName, 0666)) {
                        ssh2_disconnect($_con);
                        return "ファイル転送に失敗しました";
                    }

                    // 240*180 リサイズを行う
                    $_out2 = ImageCreateTrueColor(240, 180);
                    ImageCopyResampled($_out2, $dst,
                        0,0,0,0,240,180, 480, 360);
                    $_res2 = ImageJPEG($_out2, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res2 === true) {
                        $_workImg2 = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out2);
                    $_240_keyPath = $_serverInfo['path'] . "/android/240x180/" . substr($fullStoryId, 0, 3) . "/";
                    // ディレクトリを作成する
                    ssh2_sftp_mkdir($_sftp, $_240_keyPath, 0775);

                    // ファイル転送
                    if (!ssh2_scp_send($_con, $_workImg2, $_240_keyPath . $_fileName, 0666)) {
                        ssh2_disconnect($_con);
                        return "ファイル転送に失敗しました";
                    }

                    // 192*144 リサイズを行う
                    $_out3 = ImageCreateTrueColor(192, 144);
                    ImageCopyResampled($_out3, $dst,
                        0,0,0,0,192,144, 480, 360);
                    $_res3 = ImageJPEG($_out3, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res3 === true) {
                        $_workImg3 = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out3);
                    $_192_keyPath = $_serverInfo['path'] . "/ktai/story/" . substr($fullStoryId, 0, 3) . "/";
                    // ディレクトリを作成する
                    ssh2_sftp_mkdir($_sftp, $_192_keyPath, 0775);

                    // ファイル転送
                    if (!ssh2_scp_send($_con, $_workImg3, $_192_keyPath . $_fileName, 0666)) {
                        ssh2_disconnect($_con);
                        return "ファイル転送に失敗しました";
                    }

                    ImageDestroy($dst);
                    ImageDestroy($uploadImgFile_created);

            }//縦長end
            //---横長or正方形の場合 ---
            elseif( $_width > $_height ){

                //ピクセル数が基準以下の場合は拡大。また、警告も出す(displayで制御)。
                if( $_width < 480 ){

                    //拡大率を計算する
                    $per_w = 480 / $_width;

                    //new_width,heightを作成する
                    $new_src_w = $_width * $per_w;
                    $new_src_h = $_height * $per_w;

                    //画像を拡大
                    $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                    $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                    imagecopyresampled( $uploadImgFile_created , $uploadImgFileJpeg , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $_width , $_height ); // 拡大された元画像
                    //パスを出す
                    $uploadImgFilePass = storage_path($uploadImgFile_created);
                    ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 90);

                    //-----余白入り画像作成　ここから
                    //画像ファイル名を指定
                    $_fileName = 0;
                    $_fileName = "s" . $fullStoryId . "a.jpg";
                    //元画像のサイズを取得
                    list( $_width , $_height ) = getimagesize($uploadImgFile);

                    // 保存する画像のサイズ
                    $dst_w = 480;
                    $dst_h = 360;

                    // 保存する画像に対する元画像の比率
                    $per_w   = $dst_w/$_width;
                    $per_h   = $dst_h/$_height;
                    $per_dif = $per_w - $per_h;

                    //元画像をリサイズ（width,height）
                    if($per_dif > 0){
                        $new_src_h = $_height * $per_h;
                        $new_src_w = $_width * $per_h;
                    }
                    elseif($per_dif < 0){
                        $new_src_h = $_height * $per_w;
                        $new_src_w = $_width * $per_w;
                    }
                    elseif($per_dif == 0){
                        $new_src_h = $_height * $per_h;
                        $new_src_w = $_width * $per_w;
                    }

                    //中央点の計算
                    $centerX = $new_src_w/2;
                    $centerY = $new_src_h/2;

                    //開始点の算出
                    $startPointX = $dst_w/2 - $centerX;
                    $startPointY = $dst_h/2 - $centerY;

                    //画像を縮小
                    $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                    $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                    imagecopyresampled( $uploadImgFile_created , $uploadImgFileJpeg , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $_width , $_height ); // 縮小された元画像
                    //パスを出す
                    $uploadImgFilePass = storage_path($uploadImgFile_created);
                    ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 75);

                    // 背景画像を生成する
                    $dst = imagecreatetruecolor($dst_w, $dst_h);
                    $bg_color = imagecolorallocate( $dst, 0, 0, 0 );
                    imagefilledrectangle( $dst , 0 , 0 , $dst_w , $dst_h , $bg_color ); // 白の背景を$lengthのサイズで作成
                    //パスを出す
                    $bgPass = storage_path($dst);
                    ImageJPEG($dst, $bgPass, 75);

                    //合成する
                    imagecopy( $dst , $uploadImgFile_created ,$startPointX  , $startPointY , 0 , 0  , $new_src_w , $new_src_h );

                    //---編集画像のPassを取得
                    $filePass = storage_path($dst);
                    //---画像容量を落とす(205kb以下)
                    ImageJPEG($dst, $filePass, 70);

                    // imageサーバーへ接続して保存する
                    // ディレクトリを設定
                    $_keyPath = $_serverInfo['path'] . "/android/480x360/" . substr($fullStoryId, 0, 3) . "/";
                    // ディレクトリを作成
                    ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);

                    // ファイルを転送
                    if (!ssh2_scp_send($_con, $filePass, $_keyPath . $_fileName, 0666)) {
                        return "キー480x360のファイル転送に失敗しました横長or正方形の場合 ///1400*2100（2:3画像）///を作成しない";
                    }

                    // 320*240 リサイズを行う
                    $_workImg = $filePass;
                    $_out = ImageCreateTrueColor(320, 240);
                    ImageCopyResampled($_out, $dst,
                        0,0,0,0, 320, 240, 480, 360);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                    $_320_keyPath = $_serverInfo['path'] . "/android/320x240/" . substr($fullStoryId, 0, 3) . "/";

                    // ディレクトリを作成する
                    ssh2_sftp_mkdir($_sftp, $_320_keyPath, 0775);

                    // ファイル転送
                    if (!ssh2_scp_send($_con, $_workImg, $_320_keyPath . $_fileName, 0666)) {
                        ssh2_disconnect($_con);
                        return "ファイル転送に失敗しました";
                    }

                    // 240*180 リサイズを行う
                    $_out2 = ImageCreateTrueColor(240, 180);
                    ImageCopyResampled($_out2, $dst,
                        0,0,0,0,240,180, 480, 360);
                    $_res2 = ImageJPEG($_out2, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res2 === true) {
                        $_workImg2 = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out2);
                    $_240_keyPath = $_serverInfo['path'] . "/android/240x180/" . substr($fullStoryId, 0, 3) . "/";
                    // ディレクトリを作成する
                    ssh2_sftp_mkdir($_sftp, $_240_keyPath, 0775);

                    // ファイル転送
                    if (!ssh2_scp_send($_con, $_workImg2, $_240_keyPath . $_fileName, 0666)) {
                        ssh2_disconnect($_con);
                        return "ファイル転送に失敗しました";
                    }

                    // 192*144 リサイズを行う
                    $_out3 = ImageCreateTrueColor(192, 144);
                    ImageCopyResampled($_out3, $dst,
                        0,0,0,0,192,144, 480, 360);
                    $_res3 = ImageJPEG($_out3, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res3 === true) {
                        $_workImg3 = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out3);
                    $_192_keyPath = $_serverInfo['path'] . "/ktai/story/" . substr($fullStoryId, 0, 3) . "/";
                    // ディレクトリを作成する
                    ssh2_sftp_mkdir($_sftp, $_192_keyPath, 0775);

                    // ファイル転送
                    if (!ssh2_scp_send($_con, $_workImg3, $_192_keyPath . $_fileName, 0666)) {
                        ssh2_disconnect($_con);
                        return "ファイル転送に失敗しました";
                    }
                    continue;

                }
                //ピクセル数が基準値以上
                elseif( 480 <= $_width ){

                    //-----余白入り画像作成
                    //画像ファイル名を指定
                    $_fileName = 0;
                    $_fileName = "s" . $fullStoryId . "a.jpg";
                    //元画像のサイズを取得
                    list( $_width , $_height ) = getimagesize($uploadImgFile);

                    // 保存する画像のサイズ
                    $dst_w = 480;
                    $dst_h = 360;

                    // 保存する画像に対する元画像の比率
                    $per_w   = $dst_w/$_width;

                    //元画像をリサイズ（width,height）
                    $new_src_h = $_height * $per_w;
                    $new_src_w = $_width * $per_w;

                    //中央点の計算
                    $centerX = $new_src_w/2;
                    $centerY = $new_src_h/2;

                    //開始点の算出
                    $startPointX = $dst_w/2 - $centerX;
                    $startPointY = $dst_h/2 - $centerY;

                    //画像を縮小
                    $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                    $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                    imagecopyresampled( $uploadImgFile_created , $uploadImgFileJpeg , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $_width , $_height ); // 縮小された元画像
                    //パスを出す
                    $uploadImgFilePass = storage_path($uploadImgFile_created);
                    ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 75);

                    // 背景画像を生成する
                    $dst = imagecreatetruecolor($dst_w, $dst_h);
                    $bg_color = imagecolorallocate( $dst, 0, 0, 0 );
                    imagefilledrectangle( $dst , 0 , 0 , $dst_w , $dst_h , $bg_color ); // 白の背景を$lengthのサイズで作成
                    //パスを出す
                    $bgPass = storage_path($dst);
                    ImageJPEG($dst, $bgPass, 75);

                    //合成する
                    imagecopy( $dst , $uploadImgFile_created , $startPointX  , $startPointY , 0 , 0  , $new_src_w , $new_src_h );

                    //---編集画像のPassを取得
                    $filePass = storage_path($dst);
                    //---画像容量を落とす(205kb以下)
                    ImageJPEG($dst, $filePass, 70);

                    // imageサーバーへ接続して保存する
                    // ディレクトリを設定
                    $_keyPath = $_serverInfo['path'] . "/android/480x360/" . substr($fullStoryId, 0, 3) . "/";

                    // ディレクトリを作成
                    ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);

                    // ファイルを転送
                    if (!ssh2_scp_send($_con, $filePass, $_keyPath . $_fileName, 0666)) {
                        ssh2_disconnect($_con);
                        return "キー480x360のファイル転送に失敗しましたピクセル数が基準値以上";
                    }

                    // 320*240 リサイズを行う
                    $_workImg = $filePass;
                    $_out = ImageCreateTrueColor(320, 240);
                    ImageCopyResampled($_out, $dst,
                        0,0,0,0, 320, 240, 480, 360);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                    $_320_keyPath = $_serverInfo['path'] . "/android/320x240/" . substr($fullStoryId, 0, 3) . "/";

                    // ディレクトリを作成する
                    ssh2_sftp_mkdir($_sftp, $_320_keyPath, 0775);

                    // ファイル転送
                    if (!ssh2_scp_send($_con, $_workImg, $_320_keyPath . $_fileName, 0666)) {
                        ssh2_disconnect($_con);
                        return "ファイル転送に失敗しました";
                    }

                    // 240*180 リサイズを行う
                    $_out2 = ImageCreateTrueColor(240, 180);
                    ImageCopyResampled($_out2, $dst,
                        0,0,0,0,240,180, 480, 360);
                    $_res2 = ImageJPEG($_out2, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res2 === true) {
                        $_workImg2 = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out2);
                    $_240_keyPath = $_serverInfo['path'] . "/android/240x180/" . substr($fullStoryId, 0, 3) . "/";
                    // ディレクトリを作成する
                    ssh2_sftp_mkdir($_sftp, $_240_keyPath, 0775);

                    // ファイル転送
                    if (!ssh2_scp_send($_con, $_workImg2, $_240_keyPath . $_fileName, 0666)) {
                        ssh2_disconnect($_con);
                        return "ファイル転送に失敗しました";
                    }

                    // 192*144 リサイズを行う
                    $_out3 = ImageCreateTrueColor(192, 144);
                    ImageCopyResampled($_out3, $dst,
                        0,0,0,0,192,144, 480, 360);
                    $_res3 = ImageJPEG($_out3, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res3 === true) {
                        $_workImg3 = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out3);
                    $_192_keyPath = $_serverInfo['path'] . "/ktai/story/" . substr($fullStoryId, 0, 3) . "/";
                    // ディレクトリを作成する
                    ssh2_sftp_mkdir($_sftp, $_192_keyPath, 0775);

                    // ファイル転送
                    if (!ssh2_scp_send($_con, $_workImg3, $_192_keyPath . $_fileName, 0666)) {
                        ssh2_disconnect($_con);
                        return "ファイル転送に失敗しました";
                    }

                }//--- ここまで ピクセル数が基準値以上

                ImageDestroy($dst);
                ImageDestroy($uploadImgFile_created);

            }//---ここまで 横長の場合---
        }
    }


    public static function resizeArtwork($full_story_id, $new_width, $new_height)
    {
        $artwork_path = public_path(IMG_TMP_DIR.$full_story_id.'.jpg');
        $artwork_info = getimagesize($artwork_path);
        $src_width = $artwork_info[0];
        $src_height = $artwork_info[1];
        $src_mime = strtolower($artwork_info['mime']);
        if ( $src_mime != 'image/jpeg' ) return false;
        $src_img=imagecreatefromjpeg($artwork_path);


        $src_ratio = $src_width/$src_height;
        $new_ratio = $new_width / $new_height;

        if ( $src_ratio >= $new_ratio ) {
            $dst_width  = $new_width;
            $dst_height = $src_height * ( $new_width / $src_width );
            $dst_x      = 0;
            $dst_y      = ( $new_height - $dst_height ) / 2;
        } else {
            $dst_height  = $new_height;
            $dst_width   = $src_width * ( $new_height / $src_height );
            $dst_x       = ( $new_width - $dst_width ) / 2;
            $dst_y       = 0;
        }

        $new_img = imagecreatetruecolor($new_width,$new_height);
        $backgroundcolor = imagecolorallocate($new_img, 0, 0, 0);
        imagefill($new_img, 0, 0, $backgroundcolor);
        // 拡大縮小
        imagecopyresized($new_img,$src_img,$dst_x,$dst_y, 0, 0, $dst_width, $dst_height, $src_width, $src_height);
        imagejpeg($new_img,public_path(IMG_TMP_DIR.$new_width.'x'.$new_height.'/'.$full_story_id.'.jpg'));
        imagedestroy($new_img);

        return true;
    }

    public static function registerArtwork($full_story_id, $artwork)
    {
        if ( !is_null($width) && !is_null($height) ) {
            $artwork->move('media/' . $width . 'x' . $height . '/', $full_story_id . '.jpg');
        } else {
            $tileArtwork = Story::find($full_story_id);
            if (is_null($tileArtwork)) {
                $tileArtwork = new Story();
                $tileArtwork->full_story_id = $full_story_id;
            }

            list($width, $height) = getimagesize($artwork->getPathName());
            $artwork->move('media/original/', $full_story_id . '.jpg');
            $tileArtwork->save();
        }
    }

    /**
     * 各話一覧画像のアップロードを行う
     * @param unknown $fullStoryId
     * @param unknown $uploadImgFile
     */
    public static function uploadStoryListImg($fullStoryId, $uploadImgFile)
    {
        //16:9画像を作成
        $_fileName = "u" . $fullStoryId . "a.jpg";
        $new_width = 1920;
        $new_height = 1080;
        $background_color = 0; //黒
        $_detail_path = "/dev/android/1920x1080/";

        $_16_9_process = ImgProcess::process($fullStoryId, $uploadImgFile, $new_width, $new_height, $background_color, $_fileName, $_detail_path);

        //元画像から4:3画像を作成
        $_fileName = "s" . $fullStoryId . "a.jpg";
        $new_width = 640;
        $new_height = 480;
        $background_color = 0; //黒
        $_detail_path = "/dev/android/";

        $_process = ImgProcess::process($fullStoryId, $uploadImgFile, $new_width, $new_height, $background_color, $_fileName, $_detail_path);

        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 画像サイズを取得する
            $_img = $_process;
            $_width = ImageSx($_img);
            $_height = ImageSy($_img);

            //----- 480x360向け
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(480, 360);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,480,360, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);

                $_path = $_serverInfo['path'] . "/android/480x360/" . substr($fullStoryId, 0, 3) . "/";

                // ディレクトリを作成する
                ssh2_sftp_mkdir($_sftp, $_path, 0775);

                // ファイル転送
                if (!ssh2_scp_send($_con, $_workImg, $_path . $_fileName, 0666)) {
                    ssh2_disconnect($_con);
                    return "ファイル転送に失敗しました";
                }
            //----- 480x360向け

            //----- 320x240向け
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(320, 240);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,320,240, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);

                $_path = $_serverInfo['path'] . "/android/320x240/" . substr($fullStoryId, 0, 3) . "/";

                // ディレクトリを作成する
                ssh2_sftp_mkdir($_sftp, $_path, 0775);

                // ファイル転送
                if (!ssh2_scp_send($_con, $_workImg, $_path . $_fileName, 0666)) {
                    ssh2_disconnect($_con);
                    return "ファイル転送に失敗しました";
                }
            //----- 320x240向け

            //----- 240x180向け
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(240, 180);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,240,180, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);

                $_path = $_serverInfo['path'] . "/android/240x180/" . substr($fullStoryId, 0, 3) . "/";

                // ディレクトリを作成する
                ssh2_sftp_mkdir($_sftp, $_path, 0775);

                // ファイル転送
                if (!ssh2_scp_send($_con, $_workImg, $_path . $_fileName, 0666)) {
                    ssh2_disconnect($_con);
                    return "ファイル転送に失敗しました";
                }
            //-----  240x180向け

            //----- 192x144向け
                    // リサイズを行う
                    $_out = ImageCreateTrueColor(192, 144);
                    ImageCopyResampled($_out, $_img,
                        0,0,0,0,192,144, $_width, $_height);
                    $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
                    if ($_res === true) {
                        $_workImg = IMG_TMP_DIR . $_fileName;
                    }
                    ImageDestroy($_out);
                $_path = $_serverInfo['path'] . "/ktai/story/" . substr($fullStoryId, 0, 3) . "/";

                // ディレクトリを作成する
                ssh2_sftp_mkdir($_sftp, $_path, 0775);

                // ファイル転送
                if (!ssh2_scp_send($_con, $_workImg, $_path . $_fileName, 0666)) {
                    ssh2_disconnect($_con);
                    return "ファイル転送に失敗しました";
                }
            //-----  192x144向け
        } //foreach
    }
/////end//各話一覧画像のアップロードを行う///////////////////////////////////////////////////////////////////////////////////////////////


    //各話編集ページでoriginal画像のアップロード処理を行う
     static function uploadUniversalSearchStoryImg($fullStoryId, $uploadImgFile)
    {

        $_fileName = "o" . $fullStoryId . "a.jpg";

        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);
            //---編集画像のPassを取得
            $filePass = $uploadImgFile;
            //元画像のサイズを取得
            list( $src_w , $src_h ) = getimagesize($uploadImgFile);
            //---縦長の場合---
                //高さは480未満の場合、比率に合わせて伸ばす
                if ( ($src_w < $src_h) && ($src_w < 480) ){
                  //拡大率を計算する

                  $per_w = 480 / $src_w;

                  $new_src_w = $src_w * $per_w;
                  $new_src_h = $src_h * $per_w;
                  //画像を拡大
                  $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                  $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                  imagecopyresampled( $uploadImgFile_created , $uploadImgFileJpeg , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $src_w , $src_h ); // 拡大された元画像
                  //パスを出す
                  $uploadImgFilePass = storage_path($uploadImgFile_created);
                  ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 90);

                  // imageサーバーへ接続して保存する
                  // ディレクトリを設定
                  $_keyPath = $_serverInfo['path'] . "/originalStory/" . substr($fullStoryId, 0, 3) . "/";
                  // ディレクトリを作成
                  ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);

                  // ファイルを転送
                  if (!ssh2_scp_send($_con, $uploadImgFilePass, $_keyPath . $_fileName, 0666)) {
                        return "ファイル転送に失敗しました";
                  }
                  ImageDestroy($uploadImgFile_created);
                  continue;

                }
                // 横長でw480未満の場合
                elseif ( ($src_h < $src_w) && ($src_h < 480) ){

                  //拡大率を計算する
                  $per_h = 480 / $src_h;

                  $new_src_w = $src_w * $per_h;
                  $new_src_h = $src_h * $per_h;

                  //画像を拡大
                  $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                  $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                  imagecopyresampled( $uploadImgFile_created , $uploadImgFileJpeg , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $src_w , $src_h ); // 拡大された元画像
                  //パスを出す
                  $uploadImgFilePass = storage_path($uploadImgFile_created);
                  ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 90);

                  // imageサーバーへ接続して保存する
                  // ディレクトリを設定
                  $_keyPath = $_serverInfo['path'] . "/originalStory/" . substr($fullStoryId, 0, 3) . "/";
                  // ディレクトリを作成
                  ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);

                  // ファイルを転送
                  if (!ssh2_scp_send($_con, $uploadImgFilePass, $_keyPath . $_fileName, 0666)) {
                        return "ファイル転送に失敗しました";
                  }
                  ImageDestroy($uploadImgFile_created);
                  continue;

                }
                // 正方形の場合
                elseif ( ($src_h = $src_w) && ($src_w < 480) ){

                  //拡大率を計算する
                  $per_w = 480 / $src_w;

                  $new_src_w = $src_w * $per_w;
                  $new_src_h = $src_h * $per_w;

                  //画像を拡大
                  $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                  $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                  imagecopyresampled( $uploadImgFile_created , $uploadImgFileJpeg , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $src_w , $src_h ); // 拡大された元画像
                  //パスを出す
                  $uploadImgFilePass = storage_path($uploadImgFile_created);
                  ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 90);

                  // imageサーバーへ接続して保存する
                  // ディレクトリを設定
                  $_keyPath = $_serverInfo['path'] . "/originalStory/" . substr($fullStoryId, 0, 3) . "/";
                  // ディレクトリを作成
                  ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);

                  // ファイルを転送
                  if (!ssh2_scp_send($_con, $uploadImgFilePass, $_keyPath . $_fileName, 0666)) {
                      return "ファイル転送に失敗しました";
                  }
                  ImageDestroy($uploadImgFile_created);
                  continue;

                }
                else{
                  //imageサーバーへ接続して保存する
                  // ディレクトリを設定
                  $_keyPath = $_serverInfo['path'] . "/originalStory/" . substr($fullStoryId, 0, 3) . "/";
                  // ディレクトリを作成
                  ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);
                  // ファイルを転送
                  if (!ssh2_scp_send($_con, $filePass, $_keyPath . $_fileName, 0666)) {
                      return "ファイル転送に失敗しました";
                  }

                }
        }//foreach end
    }


    /**キー画像登録
     * オリジナル画像のアップロード、再編集処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @return string
     */
    public static function updateOriginalImg($titleId, $uploadImgFile)
    {
        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {

            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);
            //画像ファイル名を指定
            $_fileName = "o" . $titleId . ".jpg";
            //----- オリジナルキー画像登録
            //元画像のサイズを取得
            list( $src_w , $src_h ) = getimagesize($uploadImgFile);
            //---縦長と正方形の場合---
            if( $src_w <= $src_h ){
                //高さは480未満の場合、比率に合わせて伸ばす
                if(  $src_h < 480 ){

                    //拡大率を計算する
                    $per_w = 480 / $src_w;

                    $new_src_w = $src_w * $per_w;
                    $new_src_h = $src_h * $per_w;

                    //画像を拡大
                    $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                    $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                    imagecopyresampled( $uploadImgFile_created , $uploadImgFileJpeg , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $src_w , $src_h ); // 拡大された元画像
                    //パスを出す
                    $uploadImgFilePass = storage_path($uploadImgFile_created);
                    ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 90);

                    // imageサーバーへ接続して保存する
                    // ディレクトリを設定
                    $_keyPath = $_serverInfo['path'] . "/original/" . substr($titleId, 0, 3) . "/";
                    // ディレクトリを作成
                    ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);

                    // ファイルを転送
                    if (!ssh2_scp_send($_con, $uploadImgFilePass, $_keyPath . $_fileName, 0666)) {
                        return "ファイル転送に失敗しました";
                    }
                    ImageDestroy($uploadImgFile_created);
                    continue;

                }else{
                  //imageサーバーへ接続して保存する
                  // ディレクトリを設定
                  $_keyPath = $_serverInfo['path'] . "/original/" . substr($titleId, 0, 3) . "/";
                  // ディレクトリを作成
                  ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);
                  // ファイルを転送
                  if (!ssh2_scp_send($_con, $uploadImgFile, $_keyPath . $_fileName, 0666)) {
                      return "ファイル転送に失敗しました";
                  }
                }
            }//---ここまで 縦長の場合---end
            // 横長の場合
            elseif($src_w > $src_h){
                //横さは480未満の場合、比率に合わせて伸ばす
                if(  $src_w < 480 ){

                    //拡大率を計算する
                    $per_h = 480 / $src_h;

                    $new_src_w = $src_w * $per_h;
                    $new_src_h = $src_h * $per_h;

                    //画像を拡大
                    $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                    $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                    imagecopyresampled( $uploadImgFile_created , $uploadImgFileJpeg , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $src_w , $src_h ); // 拡大された元画像
                    //パスを出す
                    $uploadImgFilePass = storage_path($uploadImgFile_created);
                    ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 90);

                    // imageサーバーへ接続して保存する
                    // ディレクトリを設定
                    $_keyPath = $_serverInfo['path'] . "/original/" . substr($titleId, 0, 3) . "/";
                    // ディレクトリを作成
                    ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);

                    // ファイルを転送
                    if (!ssh2_scp_send($_con, $uploadImgFilePass, $_keyPath . $_fileName, 0666)) {
                        return "ファイル転送に失敗しました";
                    }
                    ImageDestroy($uploadImgFile_created);
                    continue;

                }else{
                  //imageサーバーへ接続して保存する
                  // ディレクトリを設定
                  $_keyPath = $_serverInfo['path'] . "/original/" . substr($titleId, 0, 3) . "/";
                  // ディレクトリを作成
                  ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);
                  // ファイルを転送
                  if (!ssh2_scp_send($_con, $uploadImgFile, $_keyPath . $_fileName, 0666)) {
                      return "ファイル転送に失敗しました";
                  }
                }
            }//横長end
        }//foreach ここまで（固定）
   }//オリジナル画像のアップロード、再編集処理を行う ここまで（固定）

    /**
     * UniversalSearch用キー画像のアップロード、再編集処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @return string
     */
    public static function updateAppleAImg($titleId, $uploadImgFile)
    {

      $_fileName = "US23_" . $titleId . ".jpg";

      // 配備先サーバ分ループする
      foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
          $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
          if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
              return $_serverInfo['host']."への認証に失敗しました";
          }
          // SFTPを取得
          $_sftp = ssh2_sftp($_con);

          list( $src_w , $src_h ) = getimagesize($uploadImgFile);

            if( $src_w < $src_h ){

                //ピクセル数が基準に達していない場合は拡大する。また、警告も出す。
                if( $src_h <> 2100 || $src_w <> 1400 ){

                    $per_h = $src_h / 2100;
                    $per_w = $src_w / 1400;
                    //足りない方を基準にして伸ばす
                    // 拡大する比率を決める
                    // 幅を基準
                    if ($per_w < $per_h){
                        $new_h = ($src_h * 1400) / $src_w;
                        $new_w = 1400;
                        $crop_x = 0;
                        $crop_y = ceil((2100 - $new_h) / 2);
                    }else{
                        $new_w = ($src_w * 2100) / $src_h;
                        $new_h = 2100;
                        $crop_x = ceil((1400 - $new_w) / 2);
                        $crop_y = 0;
                    }

                    //画像を拡大
                    $uploadImgFile_extend = imagecreatetruecolor(1400, 2100);
                    $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                    imagecopyresampled( $uploadImgFile_extend , $uploadImgFileJpeg , $crop_x , $crop_y , 0 , 0 , $new_w , $new_h , $src_w , $src_h ); // 拡大された元画像
                    //パスを出す
                    $uploadImgFilePass = storage_path($uploadImgFile_extend);
                    ImageJPEG($uploadImgFile_extend, $uploadImgFilePass, 90);

                    $_fileName = 0;
                    $_fileName = "US23_" . $titleId . ".jpg";
                    // imageサーバーへ接続して保存する
                    // ディレクトリを設定
                    $_keyPath = $_serverInfo['path'] . "/appletv/apple_A/" . substr($titleId, 0, 3) . "/";
                    // ディレクトリを作成
                    ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);

                    // ファイルを転送
                    if (!ssh2_scp_send($_con, $uploadImgFilePass, $_keyPath . $_fileName, 0666)) {
                        return "1400*2100のファイル転送に失敗しました";
                    }


                }else{
                    //拡大不要の場合
                    // 画像ファイル名を指定
                    $_fileName = 0;
                    $_fileName = "US23_" . $titleId . ".jpg";
                    // imageサーバーへ接続して保存する
                    // ディレクトリを設定
                    $_keyPath = $_serverInfo['path'] . "/appletv/apple_A/" . substr($titleId, 0, 3) . "/";
                    // ディレクトリを作成
                    ssh2_sftp_mkdir($_sftp, $_keyPath, 0755);

                    // ファイルを転送
                    if (!ssh2_scp_send($_con, $uploadImgFile, $_keyPath . $_fileName, 0666)) {
                        return "1400*2100のファイル転送に失敗しました";
                    }
                    // ImageDestroy($uploadImgFile);
                    // continue;
                }
            }
       }// foreach end

    }

    /**
     * UniversalSearch用キー画像のアップロード、再編集処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @return string
     */
    public static function updateAppleBImg($titleId, $uploadImgFile)
    {

      $_fileName = "US11_" . $titleId . ".jpg";

      // 配備先サーバ分ループする
      foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
          $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
          if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
              return $_serverInfo['host']."への認証に失敗しました";
          }
          // SFTPを取得
          $_sftp = ssh2_sftp($_con);

          // 画像サイズを取得する
          $_img = ImageCreateFromJPEG($uploadImgFile);
          $_width = ImageSx($_img);
          $_height = ImageSy($_img);

          if ($_width == "1400" && $_height == "1400") {
              // //imageサーバーへ接続して保存する
              $_KeyPath = $_serverInfo['path'] . "/appletv/apple_B/" . substr($titleId, 0, 3) . "/";
              // ディレクトリを作成
              ssh2_sftp_mkdir($_sftp, $_KeyPath, 0755);

              // ファイルを転送
              if (!ssh2_scp_send($_con, $uploadImgFile, $_KeyPath . $_fileName, 0666)) {
                  return "UniversalSearch用キー画像のファイル転送に失敗しました(PC)";
              }
          }
      }// foreach end
    }

    /**
     * タイトル画像(a480)のアップロード処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @return string
     */
    public static function uploadTitleImg($titleId, $uploadImgFile)
    {
        $_fileName = "a" . $titleId . ".jpg";
        $_workImg = "";
        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 画像サイズを取得する
            $_img = ImageCreateFromJPEG($uploadImgFile);
            $_width = ImageSx($_img);
            $_height = ImageSy($_img);
            // 縦長と正方形の場合
            // if( $_width <= $_height ){

                //拡大率を計算する
                $per_h = 480 / $_height;
                $per_w = 480 / $_width;

                //値が大きい方で乗算する
                if($per_h < $per_w){
                    //new_width,heightを作成する
                    $new_src_w = $_width * $per_w;
                    $new_src_h = $_height * $per_w;
                }
                elseif($per_w < $per_h){
                    $new_src_w = $_width * $per_h;
                    $new_src_h = $_height * $per_h;
                }
                elseif($per_w == $per_h){
                    $new_src_w = $_width * $per_h;
                    $new_src_h = $_height * $per_h;
                }

                //画像を拡大
                $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                $uploadImgFileJpeg = imagecreatefromjpeg( $uploadImgFile );
                imagecopyresampled( $uploadImgFile_created , $uploadImgFileJpeg , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $_width , $_height ); // 拡大された元画像
                //パスを出す
                $uploadImgFilePass = storage_path($uploadImgFile_created);
                ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 90);

                //ファイル名を統一
                $uploadImgFile_large = $uploadImgFile_created;//拡大を確認

                //-----余白入り画像作成　ここから
                //width,heightを再計測する
                 $_width = imagesx($uploadImgFile_large);
                 $_height = imagesy($uploadImgFile_large);

                // 保存する画像のサイズ
                $dst_w = 480;
                $dst_h = 480;
                if( $_width <= $_height ){
                    // 保存する画像に対する元画像の比率
                    $per_h   = $dst_h/$_height;
                   //元画像をリサイズ（width,height）
                    $new_src_w = $_width * $per_h;
                    $new_src_h = $_height * $per_h;

                    //中央点の計算
                    $centerX = $new_src_w/2;
                    $centerY = $new_src_h/2;

                    //開始点の算出
                    $startPointX = $dst_w/2 - $centerX;
                    $startPointY = 0;

                }else{
                    // 保存する画像に対する元画像の比率
                    $per_w   = $dst_w/$_width;
                   //元画像をリサイズ（width,height）
                    $new_src_w = $_width * $per_w;
                    $new_src_h = $_height * $per_w;

                    //中央点の計算
                    $centerX = $new_src_w/2;
                    $centerY = $new_src_h/2;

                    //開始点の算出
                    $startPointX = 0;
                    $startPointY = $dst_h/2 - $centerY;

                }

                //画像を縮小
                $uploadImgFile_created = imagecreatetruecolor($new_src_w, $new_src_h);
                imagecopyresampled( $uploadImgFile_created , $uploadImgFile_large , 0 , 0 , 0 , 0 , $new_src_w , $new_src_h , $_width , $_height ); // 縮小された元画像
                //パスを出す
                $uploadImgFilePass = storage_path($uploadImgFile_created);
                ImageJPEG($uploadImgFile_created, $uploadImgFilePass, 75);

                // 背景画像を生成する
                $dst = imagecreatetruecolor($dst_w, $dst_h);
                $bg_color = imagecolorallocate( $dst, 255, 255, 255 );
                imagefilledrectangle( $dst , 0 , 0 , $dst_w , $dst_h , $bg_color ); // 白の背景を$lengthのサイズで作成
                //パスを出す
                $bgPass = storage_path($dst);
                ImageJPEG($dst, $bgPass, 100);
                //合成する
                imagecopy( $dst , $uploadImgFile_created , $startPointX  , $startPointY , 0 , 0  , $new_src_w , $new_src_h );
                //---編集画像のPassを取得
                $filePass = storage_path($dst);
                //---画像容量を落とす(205kb以下)
                ImageJPEG($dst, $filePass, 95);

                //----- PC向けの処理
                // PC向けのディレクトリを設定
                $_pcPath = $_serverInfo['path'] . "/vm_pc/480x480/" . substr($titleId, 0, 3) . "/";
                // ディレクトリを作成
                ssh2_sftp_mkdir($_sftp, $_pcPath, 0755);
                // ファイルを転送
                if (!ssh2_scp_send($_con, $filePass, $_pcPath . $_fileName, 0666)) {
                    return "キーa480のファイル転送に失敗しました(PC)";
                }
                //----- PC向けここまで

                //----- スマホ向けの処理
                $_img = ImageCreateFromJPEG($filePass);
                $_width = ImageSx($_img);
                $_height = ImageSy($_img);
                $_out = ImageCreateTrueColor($_width, $_height);
                ImageCopyResampled($_out, $_img, 0, 0, 0, 0, $_width, $_height, $_width, $_height);
                // 作業ディレクトリにファイル出力
                $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 85);
                if ($_res == true) {
                    $_workImg = IMG_TMP_DIR . $_fileName;
                }
                // メモリを解放
                ImageDestroy($_out);
                // スマホ向けディレクトリを設定
                $_smtPath = $_serverInfo['path'] . "/android/480x480/" . substr($titleId, 0, 3) . "/";
                // ディレクトリを作成
                ssh2_sftp_mkdir($_sftp, $_smtPath, 0775);
                // ファイル転送
                if (!ssh2_scp_send($_con, $_workImg, $_smtPath . $_fileName, 0666)) {
                    return "キーa480のファイル転送に失敗しました(smt)";
                }
                if ($_workImg != "") {
                    // 作業ファイルを削除
                    exec("rm -f " . $_workImg);
                }
                //----- スマホ向けここまで

                //----- フィーチャーフォン向け処理
                $_img = ImageCreateFromJPEG($filePass);
                $_width = ImageSx($_img);
                $_height = ImageSy($_img);
                $_out = ImageCreateTrueColor($_width, $_height);
                ImageCopyResampled($_out, $_img, 0, 0, 0, 0, $_width, $_height, $_width, $_height);
                // 作業ディレクトリにファイル出力
                $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 75);
                if ($_res == true) {
                    $_workImg = IMG_TMP_DIR . $_fileName;
                }
                // メモリを解放
                ImageDestroy($_out);
                // スマホ向けディレクトリを設定
                $_smtPath = $_serverInfo['path'] . "/ktai/title/" . substr($titleId, 0, 3) . "/";
                // ディレクトリを作成
                ssh2_sftp_mkdir($_sftp, $_smtPath, 0775);
                // ファイル転送
                if (!ssh2_scp_send($_con, $_workImg, $_smtPath . $_fileName, 0666)) {
                    return "キーa480のファイル転送に失敗しました(fph)";
                }
                if ($_workImg != "") {
                    // 作業ファイルを削除
                    exec("rm -f " . $_workImg);
                }
                //----- フィーチャーフォン向け処理ここまで
            // }//縦長と正方形の場合 end

        }//foreach end
    }

    /**
     * インフォメーション用画像のアップロード処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @param unknown $mime
     * @return string
     */
    public static function uploadInfoImg($titleId, $uploadImgFile, $mime)
    {
        $_fileName = "ia" . $titleId . self::getExt($mime);

        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 配備Pathを設定
            $_path = $_serverInfo['path'] . "/infosite/480x480/" . substr($titleId, 0, 3) . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // ファイル転送
            if (!ssh2_scp_send($_con, $uploadImgFile, $_path . $_fileName, 0666)) {
                return "キーia480のファイル転送に失敗しました";
            }
        }
    }

    /**
     * サムネイル画像(b34)のアップロード処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @param unknown $mime
     */
    public static function uploadThumbnail1Img($titleId, $uploadImgFile, $mime)
    {
        $_fileName = "b" . $titleId . self::getExt($mime);
        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 配備Pathを設定
            $_path = $_serverInfo['path'] . "/ktai/thumbnail1/" . substr($titleId, 0, 3) . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // ファイル転送
            if (!ssh2_scp_send($_con, $uploadImgFile, $_path . $_fileName, 0666)) {
                return "サムネイルb32のファイル転送に失敗しました";
            }
        }
    }

    /**
     * サムネイル画像(e64)のアップロード処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @param unknown $mime
     */
    public static function uploadThumbnail2Img($titleId, $uploadImgFile, $mime)
    {
        $_fileName = "e" . $titleId . self::getExt($mime);

        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 配備Pathを設定
            $_path = $_serverInfo['path'] . "/ktai/thumbnail2/" . substr($titleId, 0, 3) . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // ファイル転送
            if (!ssh2_scp_send($_con, $uploadImgFile, $_path . $_fileName, 0666)) {
                return "サムネイルb32のファイル転送に失敗しました";
            }
        }
    }

    /**
     * サムネイル画像(f60, f120)のアップロード処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @param unknown $mime
     */
    public static function uploadThumbnail3Img($titleId, $uploadImgFile, $mime)
    {
        $_fileName = "f" . $titleId . self::getExt($mime);

        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // スマホ向けディレクトリへアップロードファイルをそのまま配置
            $_path = $_serverInfo['path'] . "/android/120x120/" . substr($titleId, 0, 3) . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // ファイル転送
            if (!ssh2_scp_send($_con, $uploadImgFile, $_path . $_fileName, 0666)) {
                return "サムネイルf120のファイル転送に失敗しました";
            }

            // 配備Pathを設定
            $_path = $_serverInfo['path'] . "/ktai/dsearch/" . substr($titleId, 0, 3) . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // 画像ファイルをリサイズ
            $_img = ImageCreateFromJPEG($uploadImgFile);
            $_out = ImageCreateTrueColor(60, 60);
            ImageCopyResampled($_out, $_img, 0, 0, 0, 0, 60, 60, 120, 120);
            // ファイル出力
            $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 85);
            if ($_res == true) {
                // ファイル転送
                if (!ssh2_scp_send($_con, IMG_TMP_DIR . $_fileName, $_path . $_fileName, 0666)) {
                    return "サムネイルf60のファイル転送に失敗しました";
                }
            }
            ImageDestroy($_img);
        }
    }

    /**
     * バナー画像(d240, d480)のアップロード処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @param unknown $mime
     */
    public static function uploadBannerImg($titleId, $uploadImgFile, $mime)
    {
        $_fileName = "d" . $titleId . self::getExt($mime);

        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // スマホ向けディレクトリへアップロードファイルをそのまま配置
            $_path = $_serverInfo['path'] . "/android/480x96/" . substr($titleId, 0, 3) . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // ファイル転送
            if (!ssh2_scp_send($_con, $uploadImgFile, $_path . $_fileName, 0666)) {
                return "バナーd480のファイル転送に失敗しました";
            }

            // 配備Pathを設定
            $_path = $_serverInfo['path'] . "/ktai/banner/" . substr($titleId, 0, 3) . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // 画像ファイルをリサイズ
            $_img = ImageCreateFromJPEG($uploadImgFile);
            $_out = ImageCreateTrueColor(240, 48);
            ImageCopyResampled($_out, $_img, 0, 0, 0, 0, 240, 48, 480, 96);
            // ファイル出力
            $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
            if ($_res == true) {
                // ファイル転送
                if (!ssh2_scp_send($_con, IMG_TMP_DIR . $_fileName, $_path . $_fileName, 0666)) {
                    return "バナーd240のファイル転送に失敗しました";
                }
            }
            ImageDestroy($_img);
        }
    }

    /**
     * DMenu用画像(g120)のアップロード処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @param unknown $mime
     */
    public static function uploadDMenuImg($titleId, $uploadImgFile, $mime)
    {
        $_fileName = "g" . $titleId . self::getExt($mime);

        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 配備Pathを設定
            $_path = $_serverInfo['path'] . "/android/120x90/" . substr($titleId, 0, 3) . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // 画像ファイルをリサイズ
            $_img = ImageCreateFromJPEG($uploadImgFile);
            $_out = ImageCreateTrueColor(120, 90);
            ImageCopyResampled($_out, $_img, 0, 0, 0, 0, 120, 90, 240, 180);
            // ファイル出力
            $_res = ImageJPEG($_out, IMG_TMP_DIR . $_fileName, 87);
            if ($_res == true) {
                // ファイル転送
                if (!ssh2_scp_send($_con, IMG_TMP_DIR . $_fileName, $_path . $_fileName, 0666)) {
                    return "ボックスg120のファイル転送に失敗しました";
                }
            }
            ImageDestroy($_img);
        }
    }

    /**
     * ハイバナー(h1280)のアップロード処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @param unknown $mime
     */
    public static function uploadHighBannerImg($titleId, $uploadImgFile, $mime)
    {
        $_fileName = "h" . $titleId . self::getExt($mime);

        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 配備Pathを設定
            $_path = $_serverInfo['path'] . "/vm_pc/1280x500/" . substr($titleId, 0, 3) . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // ファイル転送
            if (!ssh2_scp_send($_con, $uploadImgFile, $_path . $_fileName, 0666)) {
                return "ハイバナー(h1280)のファイル転送に失敗しました";
            }
        }
    }

    /**
     * PC用作品タイトル(m4:3)のアップロード処理を行う
     * @param unknown $titleId
     * @param unknown $uploadImgFile
     * @param unknown $mime
     */
    public static function uploadPcTitleImg($titleId, $uploadImgFile, $mime)
    {
            $_imgFilePc = new ImageFileTitle($uploadImgFile);
            $_imgFileHd = new ImageFileTitle($uploadImgFile);
            $_imgFile4k = new ImageFileTitle($uploadImgFile);

        try{

            $_fileName = self::createTitleImageFileName(ImageFileConfig::IMG_TYPE_PC_TITLE, $titleId);
            $_fileName_hd = self::createTitleImageFileName(ImageFileConfig::IMG_TYPE_PC_TITLE_HD, $titleId);
            $_fileName_4k = self::createTitleImageFileName(ImageFileConfig::IMG_TYPE_PC_TITLE_4K, $titleId);

            // PC用作品タイトル(m4:3)
            // $mimeで出力するファイルの拡張子を変動できるよう、ここで扱うファイル名には拡張子を付けない
            $_imgFilePc->create(ImageFileConfig::IMG_TYPE_PC_TITLE, $mime, IMG_TMP_DIR_4x3 , $_fileName);

            // COCORO VIDEO/本店AndroidTV 面だし用画像(HDディスプレイ)_2018/04追加
            $_imgFileHd->create(ImageFileConfig::IMG_TYPE_PC_TITLE_HD, $mime, IMG_TMP_DIR_4x3_HD , $_fileName_hd);

            // COCORO VIDEO/本店AndroidTV 面だし用画像(4Kディスプレイ)_2018/04追加
            $_imgFile4k->create(ImageFileConfig::IMG_TYPE_PC_TITLE_4K, $mime, IMG_TMP_DIR_4x3_4K , $_fileName_4k);

            // 配備Pathを作成
            // $imgDeployList のpathがサーバー毎に異なっても対応できるように
            // ファイルパスに親ディレクトリをappendできる機能をScpObjに実装している
            $_ext = self::getExt($mime);

            // TODO 以下のパス、他箇所もまとめてDefineしたい
            $_path_pc = self::createImageDstPath(ImageFileConfig::IMG_TYPE_PC_TITLE, $titleId, $_fileName, $mime);
            $_path_hd = self::createImageDstPath(ImageFileConfig::IMG_TMP_DIR_4x3_HD, $titleId, $_fileName, $mime);
            $_path_4k = self::createImageDstPath(ImageFileConfig::IMG_TMP_DIR_4x3_4K, $titleId, $_fileName, $mime);

            $_dir_mode = ImageFileConfig::DEFAULT_IMG_DIR_MODE;
            $_file_mode = ImageFileConfig::DEFAULT_IMG_FILE_MODE;

            // SCP送信用のオブジェクトを作成(ファイルにつき１つ)
            // 送信先ディレクトリはsetDstPath()、appendDstParentDir()で後から調整できる
            $_scp_obj_pc = new ScpObj($_imgFilePc, $_path_pc, $_dir_mode, $_file_mode);
            $_scp_obj_hd = new ScpObj($_imgFileHd, $_path_hd, $_dir_mode, $_file_mode);
            $_scp_obj_4k = new ScpObj($_imgFile4k, $_path_4k, $_dir_mode, $_file_mode);

            // SCP送信のオブジェクトを配列にする
            $_scp_objs = new ArrayObject(array($_scp_obj_pc, $_scp_obj_hd, $_scp_obj_4k));

            // 送信
            self::uploadImgFile2ImgServer($_scp_objs);

            // 一時ファイルを削除
            // TODO アップロードされてきたファイルは勝手に消える？
            // $_imgFilePc->removeFile(); 
            $_imgFileHd->removeFile();
            $_imgFile4k->removeFile();
        }
        catch (Exception $e)
        {
            // 例外発生時は一時ファイルを消して終わる
            $_imgFileHd->removeFile();
            $_imgFile4k->removeFile();

            $_msg = sprintf("PC用作品タイトル(m4:3)のアップロード処理に失敗しました。 原因:%s \n", $e->getMessage());

            return $_msg;
        }
    }

    /**
     * ファイル名(拡張子抜き)を作成する
     * @param  [type] $imgType [description]
     * @param  [type] $titleID [description]
     * @return [type]          [description]
     */
    public static function createTitleImageFileName($imgType, $titleID)
    {
        $_ret = "";

        //TODO 他の画像タイプについても以下を参考に追加すること
        if ($imgType === ImageFileConfig::IMG_TYPE_PC_TITLE 
            || ImageFileConfig::IMG_TYPE_PC_TITLE_HD 
            || ImageFileConfig::IMG_TYPE_PC_TITLE_4K)
        {
            $_ret = "m" . $titleID;
        }
        else 
        {
            $_ret = $titleID;
        }

        return $_ret;
    }

    /**
     * 画像を送信するリモートのパスを生成する
     * @param  [type] $imgType  [description]
     * @param  [type] $titleID  [description]
     * @param  [type] $filename [description]
     * @param  [type] $mime     [description]
     * @return [type]           [description]
     */
    private static function createImageDstPath($imgType, $titleID, $fileName, $mime)
    {
        $_ret = "";

        $_ext = self::getExt($mime);
        
        $_type_prefix = self::getDstImagePathPrefix($imgType);
        
        $_publisher_id = self::getPublisherIDfromTitleID($titleID);

        if (substr($_type_prefix, -1) === "/" )
        {
            $_ret = $_type_prefix . $_publisher_id . "/". $_fileName . $_ext;
        }
        else
        {
            $_ret = $_type_prefix . "/" . $_publisher_id . "/". $_fileName . $_ext;
        }

        return $_ret;
    }

    /**
     * 画像の送信先フォルダパスの一部(画像毎に異なる部分)を取得する
     * @param  [type] $imgType [description]
     * @return [type]          [description]
     */
    /* */
    public static function getDstImagePathPrefix($imgType)
    {
        $_ret = "";

        //TODO 他のパスについても以下を参考に追加すること
        if ($imgType === ImageFileConfig::IMG_TYPE_PC_TITLE)
        {
            $_ret = ImageFileConfig::IMG_TYPE_PC_TITLE_PATH;
        }
        elseif ($imgType === ImageFileConfig::IMG_TYPE_PC_TITLE_HD)
        {
            $_ret = ImageFileConfig::IMG_TYPE_PC_TITLE_HD_PATH;
        }
        elseif ($imgType === ImageFileConfig::IMG_TYPE_PC_TITLE_4K)
        {
            $_ret = ImageFileConfig::IMG_TYPE_PC_TITLE_4K_PATH;
        }

        return $_ret;
    }

    /**
     * 作品IDから版元IDを切り出す。
     * 引数の先頭３文字が版元IDとなる。
     * 長さが3文字未満の場合、引数をそのまま返す。
     * 引数の先頭３文字を切り出しているだけでDBとの比較チェックはしていないため注意。
     * @param  string $titleID 作品ID
     * @return string 版元ID
     */
    private static function getPublisherIDfromTitleID($titleID)
    {
        $_ret = "";

        if ( 3 <= mb_strlen($titleID))
        {
            // 先頭から３文字が版元のID
            $_ret = substr($titleId, 0, 3);
        }
        else
        {
            $_ret = $titldID;
        }
        return $_ret;
    }

    /**
     * 引数のArrayObjectからをScpObjをひとつづつ取り出し、
     * ProgramFacade::$imgDeployListで定義されているサーバへ送信する
     * @param  ArrayObject $scp_objs ScpObjを要素に持つArrayObject
     */
    private static function uploadImgFile2ImgServer($scp_objs)
    {
        $_connection =　null;

        try {
            // 配備先サーバ分ループする
            foreach (ProgramFacade::$imgDeployList as $_serverInfo)
            {
                $_connection = new RemoteConnection($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22, $_serverInfo['login'], $_serverInfo['password'] ); 

                $_connection->openSSH2Connection();

                // 渡されたオブジェクトを1つずつ送信する
                foreach($scp_objs as $_scp_obj)
                {
                    // 送信先パスに親ディレクトリを設定(/image/image.jpeg -> $_serverInfo['path']//image/image.jpeg)
                    $_scp_obj->appendDstParentDir($_serverInfo['path']);

                    // 送信先の格納ディレクトリを作成
                    $_connection->createRemoteDirectory($_scp_obj);

                    // ファイルを送信
                    $_connection->sendFile($_scp_obj);
                }

                $_connection->closeSSH2Connection();
            }
        }
        catch (Exception $e)
        {
            if(isset($_connection))
            {
                $_connection->closeSSH2Connection();
            }            
            throw $e;
        }
    }

    public static function uploadDigitalScreenImg($titleId, $uploadImgFile, $mime)
    {
        $_fileName = "ds" . $titleId . self::getExt($mime);

        // 配備先サーバ分ループする
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }
            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 配備Pathを設定
            $_path = $_serverInfo['path'] . "/digitalScreen/" . substr($titleId, 0, 3) . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // ファイル転送
            if (!ssh2_scp_send($_con, $uploadImgFile, $_path . $_fileName, 0666)) {
                return "デジタルスクリーン(ds576)のファイル転送に失敗しました";
            }
        }
    }

    /**
     * キャスト画像をイメージサーバへ配備する
     * @param unknown $titleInfo
     * @param unknown $uploadImgFile
     * @param unknown $fileName
     * @return string
     */
    public static function uploadCastImg($titleInfo, $uploadImgFile, $fileName)
    {
        // キャスト画像の配備先分ループする
        foreach (ProgramFacade::$castImgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }

            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 配備PATHを設定
            $_path = $_serverInfo['path'] . substr($titleInfo->genre_id, 1,2) . "/" . $titleInfo->full_title_id . "/";
            // ディレクトリを作成
            ssh2_sftp_mkdir($_sftp, $_path, 0775);
            // ファイルを転送
            if (!ssh2_scp_send($_con, $uploadImgFile, $_path . $fileName, 0666)) {
                return "キャスト画像のファイル転送に失敗しました";
            }
        }
    }

    /**
     * 機器関連画像をイメージサーバへ配備する
     */
    public static function uploadMachineImg($dirName, $fileName, $uploadImgFile)
    {
        foreach (ProgramFacade::$imgDeployList as $_serverInfo) {
            $_con = ssh2_connect($_serverInfo['host'], isset($_serverInfo['port']) ? $_serverInfo['port'] : 22);
            if (!ssh2_auth_password($_con, $_serverInfo['login'], $_serverInfo['password'])) {
                return $_serverInfo['host']."への認証に失敗しました";
            }

            // SFTPを取得
            $_sftp = ssh2_sftp($_con);

            // 配備PATHを設定
            $_path = $_serverInfo['path'] . "/pachi/$dirName/";

            // ファイルを転送
            if (!ssh2_scp_send($_con, $uploadImgFile, $_path . $fileName, 0666)) {
                return "画像のファイル転送に失敗しました";
            }
        }
    }

    /**
     * 画像監修用メールを送信する
     * @param unknown $titleId
     * @param unknown $mailSendFlag
     * @param unknown $mailData
     * @param unknown $comment
     */
    public static function mailSendImgCheck($titleId, $mailSendFlag, $mailData, $comment) {
        // 結果用オブジェクト
        $_ret = array(
            'ret' => false,
            'err' => "",
        );

        // タイトル情報を取得する
        $_titleInfo = Title::where('full_title_id', '=', $titleId)->first();

        // メールの送信先分ループする
        foreach ($mailData as $_mailInfo) {
            // 送信先アドレスの確認
            if (trim($_mailInfo->tanto_address) == "") {
                $_ret['ret'] = true;
                $_ret["err"] = "送信先が設定されていないため、画像確認システム用にメールは送信されませんでした";
                continue;
            }

            // 宛先を生成
            $_toAddress = "";
            $_toArray = array();
            if (trim($_mailInfo->tanto_address) != "") {
                $_toAddress = $_mailInfo->tanto_address;
                $_toArray = explode(",", $_toAddress);
            }

            // ccを作成
            $_ccAddress = "";
            $_ccArray = array();
            if (trim($_mailInfo->tanto_address_cc) != "") {
                $_ccAddress = $_mailInfo->tanto_address_cc;
                $_ccArray = explode(',', $_mailInfo->tanto_address_cc);
            }

            // フジテレビ用
            $_subject = "";
            $_body = "";
            if (substr($titleId, 0, 3) == PUBLISHER_ID_CHECK_161) {
                $_subject = IMG_CHECK_SUBJECT;
                $_body =
                    "確認用画像がアップロードされましたので、下記のURLよりご確認が可能となっております。"."\n\n".
                    "作品名：". $_titleInfo["title_name"] . " \n\n".
                    "■画像確認システム"."\n".
                    " https://manage.videomarket.jp/img-check/"."\n\n".
                    "何卒宜しくお願い致します。"."\n";
            }
            // フジ以外
            else {
                $_subject = "【ビデオマーケット作品ページご確認のお願い】" . $_mailInfo->publisher_name . "様";

                //公開開始日が未設定の場合は「未定」を設定する
                $_publishFromDate = "未定";
                if ($_titleInfo->publish_from_datetime != "") {
                    $_publishFromDate = date("Y年m月d日", strtotime($_titleInfo->publish_from_datetime));
                }

                //タイトルに対するコメント
                $_comment = "";
                if ($comment) {
                    $_comment =
                        "\nこの作品に関するコメント\n" .
                        "--------------------------------------------------------\n" .
                        $comment .
                        "\n-------------------------------------------------------\n";
                }

                //初回チェックの場合
                if ($mailSendFlag === IMG_CHECK_MAIL_TYPE_1) {
                    $_body =
                        $_mailInfo->publisher_name . "様\n\n" .
                        "いつもお世話になっております\n\n" .
                        "下記作品の作品ページが出来上がりましたのでお知らせします。\n\n" .
                        "お忙しい中恐れ入りますが配信日前日の12：00までにご確認ください。\n" .
                        "（ただし土日祝日除く）\n\n" .
                        "・配信日　" . $_publishFromDate . "\n" .
                        "・作品名　" . $_titleInfo->title_name . "\n".
                        "・画像確認システム　https://manage.videomarket.jp/img-check/\n\n{$_comment}\n".
                        "よろしくお願いいたします。\n";
                }
                //再チェックの場合
                else {
                    $_body =
                        $_mailInfo->publisher_name . "様\n\n" .
                        "いつもお世話になっております\n\n" .
                        "下記作品の情報が更新されましたのでご確認ください。\n\n" .
                        "お忙しい中恐れ入りますが配信日前日の12：00までにご確認ください。\n" .
                        "（ただし土日祝日除く）\n\n" .
                        "・配信日　" . $_publishFromDate . "\n".
                        "・作品名　" . $_titleInfo->title_name . " \n".
                        "・画像確認システム　https://manage.videomarket.jp/img-check/\n\n{$_comment}\n".
                        "よろしくお願いいたします。\n";
                }
            }

            // メールの送信処理を行う
            Mail::send(array('text'=>'emails.imgCheck'), array("body"=>$_body), function($message) use ($_toArray, $_ccArray, $_subject) {
                $message->to($_toArray)
                    ->subject($_subject);

                if (count($_ccArray) > 0) {
                    $message->cc($_ccArray);
                }

                $message->from(IMG_CHECK_FROM, IMG_CHECK_FROM_NAME);

                $message->replyTo(array(IMG_CHECK_BCC1, IMG_CHECK_BCC2));

                $message->bcc(array(IMG_CHECK_BCC1, IMG_CHECK_BCC2));
            });



            return $_ret;
        }
    }

    /**
     * 日付フォーマットの確認を行う
     * @param unknown $s
     * @return boolean
     */
    public static function checkYmdHM($s) {
        if (preg_match("/^([0-9]{4})[-\/ \.]([01]?[0-9])[-\/ \.]([0-3]?[0-9]) ([0-2]?[0-9]):([0-5]?[0-9])$/", $s, $matches)) {
            $year = intval($matches[1], 10);
            $mon = intval($matches[2], 10);
            $day = intval($matches[3], 10);
            $result = checkdate($mon, $day, $year);
            if ($result == false) {
                return false;
            }

            $hour = intval($matches[4], 10);
            $min = intval($matches[5], 10);
            if (!($hour >= 0 && $hour <= 23)) {
                return false;
            }
            if (!($min >= 0 && $min <= 59)) {
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    }


    /**
     * 各話画像のURLを設定する
     * @param unknown $list
     */
    public static function setStoryListImgUrl(&$list)
    {

        foreach ($list as $_storyData) {
            // 1つ目の画像を確認
            if ($_storyData->story_image_1_mime != "") {
                $_storyData->story_image_url = IMG_BASE_URL
                    . STORY_LIST_IMAGE_BASE
                    . $_storyData->publisher_id . "/"
                    . "s" . $_storyData->full_story_id . "a.jpg";
            }
            else {
                $_storyData->story_image_url = "";
            }
            //オリジナルの画像を確認<--
            if ($_storyData->story_image_2_mime != "") {

                // //$_storyData->story_image_url_1920 = IMG_BASE_URL
                $_storyData->story_image_url_original = IMG_BASE_URL
                //     // . DEV_STORY_EDIT_IMAGE_BASE_1920
                   . STORY_IMG_ORIGINAL_STORY
                   . $_storyData->publisher_id . "/"
                   . "o" . $_storyData->full_story_id . "a.jpg";

            }
            else {
                $_storyData->story_image_url_original = "";
            }
        }
    }


    /**
     * 各話のサムネイル画像のURLを設定する
     * @param unknown $list
     */
    public static function setStoryListThumbnailImgUrl(&$list)
    {

	foreach ($list as $_storyData) {

		$storyId = $_storyData->full_story_id;
		$imgUrl = "http://img.vm-movie.jp/appli/thumbnail/%s/%s/%s%s%s";
		$dir01 = substr($storyId,0,3);
		$dir02 = substr($storyId,3,3);
		$prefix = "T";
		$ext = "000.png";

		$_storyData->thumbnail_img_url = sprintf($imgUrl,$dir01,$dir02,$prefix,$storyId,$ext);

		$cmd = sprintf("curl  -I -s -o /dev/null %s -w '%%{http_code}'",$_storyData->thumbnail_img_url);

		exec($cmd,$respons);

		$_storyData->thumbnail_img_respons = $respons[0];

	}
    }



    /**
     * 各話画像のURLを設定する
     * @param unknown $storyInfo
     */
    public static function setStoryEditImgUrl(&$storyInfo)
    {
        $_filePath = $storyInfo->publisher_id . "/". "s" . $storyInfo->full_story_id . "a.jpg";

        $storyInfo->story_image_url = "";
        $storyInfo->story_image_url_320 = "";
        $storyInfo->story_image_url_480 = "";
        $storyInfo->story_image_url_sp = "";
        // $storyInfo->story_image_url_1920 = "";
        $storyInfo->story_image_url_original = "";

        // mimeが登録されていることを確認
        if ($storyInfo->story_image_1_mime != "") {
            $storyInfo->story_image_url = IMG_BASE_URL
                . STORY_EDIT_IMAGE_BASE
                . $_filePath;

            // 320x240の確認
            if ($storyInfo->img_flg_320x240 == "1") {
                $storyInfo->story_image_url_320 = IMG_BASE_URL
                    . STORY_EDIT_IMAGE_BASE_320
                    . $_filePath;
            }

            // 480x360の確認
            if ($storyInfo->img_flg_480x360 == "1") {
                $storyInfo->story_image_url_480 = IMG_BASE_URL
                    . STORY_EDIT_IMAGE_BASE_480
                    . $_filePath;
            }

            // SPモード向け画像の確認
            if ($storyInfo->img_flg_480x360 == "1" || $storyInfo->img_flg_320x240 == "1") {
                $storyInfo->story_image_url_sp = IMG_BASE_URL
                    . STORY_EDIT_IMAGE_BASE_SP
                    . $_filePath;
            }

        }

        if ($storyInfo->story_image_2_mime != "") {
            // 1920x1080の確認→original
            if ($storyInfo->img_flg_original == "1") {
                $_filePath = $storyInfo->publisher_id . "/". "o" . $storyInfo->full_story_id . "a.jpg";
                //$storyInfo->story_image_url_1920 = IMG_BASE_URL
                $storyInfo->story_image_url_original = IMG_BASE_URL
                    //. DEV_STORY_EDIT_IMAGE_BASE_1920
                    . STORY_IMG_ORIGINAL_STORY
                    . $_filePath;

                    if ($tmpImg = @file_get_contents($storyInfo->story_image_url_original)) {

                        $story_size = strlen($tmpImg);

                        $story_image_data = imagecreatefromstring($tmpImg);
                        $story_width  = imagesx($story_image_data);
                        $story_height = imagesy($story_image_data);
                        // list( $width , $height ) = getimagesize($_tmpImg);
                        $story_w_h = "横幅:".$story_width."<br/>"."高さ:".$story_height;
                        $storyInfo->img_url_ori_size = round($story_size / 1024, 2);
                        $storyInfo->img_url_ori_story_w_h = $story_w_h;
                        $storyInfo->width = $story_width;
                        $storyInfo->height = $story_height;

                    }

  if (error_reporting() === 0)
   {
        // This copes with @ being used to suppress errors
        // continue script execution, skipping standard PHP error handler
        return false;
   }

          }
        }
    }

    /**
     * story_idsを整える
     * @param unknown $s
     * @return unknown
     */
    public static function storyIdsFormatter($s) {
        $s = str_replace("\r\n", ",", $s);
        $s = str_replace("\n", ",", $s);
        $s = preg_replace("@ +@", ",", $s);
        $s = preg_replace("@,+@", ",", $s);
        return $s;
    }

    /**
     * story_idsをフルストーリーIDの形に変換する
     *
     *
     */
    public static function setStoryIdsFormat($storyIds, $publisherId, $titleId)
    {
        $_storyIdList = explode(",", ProgramFacade::storyIdsFormatter($storyIds));
        $_tmpIds = "";
        foreach ($_storyIdList as $_storyId) {
            if (strlen($_storyId) == 3) {
                $_tmpIds .= $publisherId . $titleId . $_storyId;
            }
            elseif (strlen($_storyId) == 6) {
                $_tmpIds .= $publisherId . $_storyId;
            }
            elseif (strlen($_storyId) == 9) {
                $_tmpIds .= $_storyId;
            }
            if (strlen($_tmpIds) > 0) {
                $_tmpIds .= ",";
            }
        }
        return $_tmpIds;
    }

    /**
     * story_ids / pack_idsの最後の文字がカンマでなければカンマをつける。
     * スペースのみが入っていればnullを返す。
     *
     */
      public static function idsAddComma($ids)
      {

          $ids = ProgramFacade::storyIdsFormatter(trim($ids));

          if ($ids != ''){
              if (substr($ids, -1) != ',') {
                  $ids .= ',';
              }
          }
          return $ids;
      }

    /**
     * 各話情報が無効である場合の背景色
     * @var unknown
     */
    const STORY_STATUS_DISABLE_BG = "danger";

    /**
     * 各話情報が表示期間前である場合の背景色
     * @var unknown
     */
    const STORY_STATUS_BEFORE_BG = "info";

    /**
     * 各話の状態を確認し、背景色の情報を設定する
     * @param unknown $storyInfo
     */
    public static function setStoryBackGroundInfo(&$storyList)
    {
        // 現在日時を設定
        $_now = date('Y-m-d H:i:s');

        foreach ($storyList as &$_storyInfo) {
            $_storyInfo->bg_class = '';
            if ($_storyInfo->enabled == "1") {
                // 各話表示フラグがONである
                if ($_storyInfo->publish_from_datetime != '' && $_now <= $_storyInfo->publish_from_datetime) {
                    // 開始日時が指定されていて現在日時よりも後の場合
                    $_storyInfo->bg_class = self::STORY_STATUS_BEFORE_BG;
                }
            }
            else {
                // 各話表示フラグがON以外である場合
                $_storyInfo->bg_class = self::STORY_STATUS_DISABLE_BG;
            }
        }
    }


    /**
     * 半角全角文字変換処理
     *
     */

    private static $kanaCnv = array(
                  array('~','～'),
                  array('<','＜'),
                  array('>','＞'),
                  array('&','＆'),
                  array(',','，'),
                  array('-','－'),
                  array('・','･'),

            );

    private static $onlyKanaCnv = array(
                  array('~','～'),
                  array('<','＜'),
                  array('>','＞'),
                  array('&','＆'),
                  array(',','，'),
                  array('-','－'),
                  array(',','，'),
                  array(':','：'),
                  array('\*','＊'),
                  array(';','；'),
                  array('\|','｜'),
                  array('@','＠'),
                  array('\+','＋'),
                  array('\[','［'),
                  array('\]','］'),
                  array('\{','｛'),
                  array('\}','｝'),

            );

    private static $onlyTag = array(
                  array('~','～'),
                  array('<','＜'),
                  array('>','＞'),
                  array('&','＆'),
                  array('-','－'),
                  array('・','･'),

            );


    public static function textConvert($text, $option = true)
    {
        //セットした更新データを全部半角に変換
        $textCnt = mb_convert_kana($text,'ak');

        //特定の記号は対象外
        foreach(ProgramFacade::$kanaCnv as $val) {
            $search_val = "/$val[0]/";
            $without_br = "/<br[[:space:]]*\/?[[:space:]]*>/i";

            if (preg_match($search_val, $textCnt)){
                $val[0] = str_replace("\\", "", $val[0]);
                $textCnt = str_replace($val[0], $val[1], $textCnt);

                //<br>はもう一度入れ替える必要がある
                if(strstr($textCnt, '＜br /＞')){
                    $textCnt = str_replace('＜br /＞', '<br />', $textCnt);
                }
            }
        }
        return $textCnt;

    }

    //作品名、コピーライト、話数名称、話名称は別の処理(変換する記号が別)、ハイフン
    public static function onlyTextConvert($text, $option = true)
    {
        //セットした更新データを全部半角に変換しない
        $onlyTextCnt = mb_convert_kana($text,'ak');
        //特定の記号は対象外
        foreach(ProgramFacade::$onlyKanaCnv as $val_2) {
            $search_val_2 = "/$val_2[0]/";

            if (preg_match($search_val_2, $onlyTextCnt)){
                $val_2[0] = str_replace("\\", "", $val_2[0]);
                $onlyTextCnt = str_replace($val_2[0], $val_2[1], $onlyTextCnt);
            }
        }
        return $onlyTextCnt;
    }

    // 2017.2.23 CMS改修依頼
    // 作品概要編集：1.紹介 2.キャッチコピー 3.あらすじ 4.みどころ(計4項目)
    // 各話情報編集：1.あらすじ(計1項目)
    // キャスト情報編集：1.キャスト名 2.キャスト紹介文 3.出演者名称(計3項目)
    // スタッフ情報編集:1.ロール 2.名前(計2項目)
    // 半角カンマ維持
    public static function onlyTagConvert($text, $option = true)
    {
        //セットした更新データを全部半角に変換
        $onlyTagCnt = mb_convert_kana($text,'ak');
        //特定の記号は対象外
        foreach(ProgramFacade::$onlyTag as $val_3) {
            $search_val_3 = "/$val_3[0]/";

            if (preg_match($search_val_3, $onlyTagCnt)){
                $val_3[0] = str_replace("\\", "", $val_3[0]);
                $onlyTagCnt = str_replace($val_3[0], $val_3[1], $onlyTagCnt);

                //<br>はもう一度入れ替える必要がある
                if(strstr($onlyTagCnt, '＜br /＞')){
                    $onlyTagCnt = str_replace('＜br /＞', '<br />', $onlyTagCnt);
                }
            }
        }
        return $onlyTagCnt;
    }

    public static function calcImageAspectRatio($width, $height)
    {
    	$aspect = (object)[
    		'width' => 0,
    		'height' => 0,
    	];

    	if ((int)$width <= 0 || (int)$height <= 0) return $aspect;

    	$gcd = call_user_func(function($m, $n) {
    		if($n > $m) list($m, $n) = array($n, $m);

    		while($n !== 0){
    			$tmp_n = $n;
    			$n = $m % $n;
    			$m = $tmp_n;
    		}
    		return $m;
    	}, $width, $height);

    	$aspect->width   = $width  / $gcd;
    	$aspect->height = $height / $gcd;

    	return $aspect;
    }
}
