<?php
  /**
   * キーワードの検索タイプ：版元ID
   * @var unknown
   */
  define('SEARCH_TYPE_PUBLISH', '1');
  /**
   * キーワードの検索タイプ：タイトルID
   * @var unknown
   */
  define('SEARCH_TYPE_TITLE', '2');
  /**
   * キーワードの検索タイプ：フリーワード
   * @var unknown
   */
  define('SEARCH_TYPE_FREE', '3');

  /**
   * 画像URLのベース
   * @var unknown
   */
  define('IMG_BASE_URL', 'http://img.videomarket.jp');
  /**
   * 作品画像のURLパス(PC)
   * @var unknown
   */
  define('TITLE_IMG_URL_PC_BASE', '/image/vm_pc/480x480/');
  /**
   * 作品画像のURLパス(Dev_PC_APPLE_A)
   * @var unknown
   */
  define('TITLE_IMG_URL_APPLE_A', '/image/appletv/apple_A/');
  /**
   * 作品画像のURLパス(Dev_PC_APPLE_B)
   * @var unknown
   */
  define('TITLE_IMG_URL_APPLE_B', '/image/appletv/apple_B/');
  /**
   * 作品画像のURLパス(Original)
   * @var unknown
   */
  define('TITLE_IMG_URL_ORIGINAL', '/image/original/');
  /**
   * 作品画像のURLパス(SMT)
   * @var unknown
   */
  define('TITLE_IMG_URL_SMT_BASE', '/image/android/480x480/');
  /**
   * 作品画像のURLパス(FPH)
   * @var unknown
   */
  define('TITLE_IMG_URL_FPH_BASE', '/image/ktai/title/');
  /**
   * 作品画像が設定されていない場合のパス
   * @var unknown
   */
  define('TITLE_NO_IMG_PATH', '/img/icon_folder.gif');

  /**
   * GIF形式画像のmime: image/gif
   * @var unknown
   */
  define('GIF_MIME', 'image/gif');
  /**
   * JPEG形式画像のmime: image/jpeg
   * @var unknown
   */
  define('JPEG_MIME_1', 'image/jpeg');
  /**
   * JPEG形式のmime: image/pjepg
   * @var unknown
   */
  define('JPEG_MIME_2', 'image/pjpeg');
  /**
   * PNG形式のmime: image/png
   */
  define('PNG_MIME', 'image/png');


  /**
   * CMSサーバでの作業ディレクトリ
   * @var unknown
   */
  define('IMG_TMP_DIR', '/data/doc_root/cms/video/work/');
  define('IMG_TMP_DIR_4x3', '/data/doc_root/cms/video/work/4x3');
  define('IMG_TMP_DIR_4x3_HD', '/data/doc_root/cms/video/work/4x3_HD');
  define('IMG_TMP_DIR_4x3_4K', '/data/doc_root/cms/video/work/4x3_4K');

  /**
   * 画像監修メール送信タイプ：0(送信しない)
   * @var unknown
   */
  define('IMG_CHECK_MAIL_TYPE_DEFAULT', '0');
  /**
   * 画像監修メール送信タイプ：1(初回送信)
   * @var unknown
   */
  define('IMG_CHECK_MAIL_TYPE_1', '1');
  /**
   * 画像監修メール送信タイプ：2(再確認)
   * @var unknown
   */
  define('IMG_CHECK_MAIL_TYPE_2', '2');

  /**
   * 版元IDのチェック用：フジテレビ(161)
   * @var unknown
   */
  define('PUBLISHER_ID_CHECK_161', '161');

  // 画像確認システム用　メール送信設定
  /**
   * メール送信元アドレス
   * @var unknown
   */
  define('IMG_CHECK_FROM', 'img-check@videomarket.jp');
  // define('IMG_CHECK_FROM', 'sasaki.michihisa@videomarket.co.jp');
  /**
   * 送信元名
   * @var unknown
   */
  define('IMG_CHECK_FROM_NAME', 'ビデオマーケット 画像確認システム');
  /**
   * 件名
   * @var unknown
   */
  define('IMG_CHECK_SUBJECT', '【ビデオマーケット確認用画像】アップロードのお知らせ');
  /**
   * 返信先メールアドレス
   * @var unknown
   */
  define('IMG_CHECK_RETURN_MAIL', 'tech@videomarket.co.jp');
  // define('IMG_CHECK_RETURN_MAIL', 'sasaki.michihisa@videomarket.co.jp');
  /**
   * BCC１
   * @var unknown
   */
  define('IMG_CHECK_BCC1', 'hensei_team@videomarket.co.jp');
  // define('IMG_CHECK_BCC1', 'sasaki.michihisa@videomarket.co.jp');

  /**
   * BCC2
   * @var unknown
   */
  define('IMG_CHECK_BCC2', 'design_team@videomarket.co.jp');
  // define('IMG_CHECK_BCC2', 'kumazaki.yuutarou@videomarket.co.jp');
  /**
   *
   * イメージ参照先各話オリジナル画像のURLパス(Original_story)
   * @var unknown
   */
  define('STORY_IMG_ORIGINAL_STORY', '/image/originalStory/');
  /**
   * イメージ参照先
   * @var unknown
   */
  define('STORY_LIST_IMAGE_BASE', '/image/android/320x240/');
  /**
   * イメージ参照先（フィーチャーフォン）
   * @var unknown
   */
  define('STORY_EDIT_IMAGE_BASE', '/image/ktai/story/');
  /**
   * イメージ参照先（480）
   * @var unknown
   */
  define('STORY_EDIT_IMAGE_BASE_480', '/image/android/480x360/');
  /**
   * イメージ参照先（320*240)
   * @var unknown
   */
  define('STORY_EDIT_IMAGE_BASE_320', '/image/android/320x240/');
  /**
   * イメージ参照先（スマートフォン）
   * @var unknown
   */
  define('STORY_EDIT_IMAGE_BASE_SP', '/image/android/240x180/');
  /**
   * イメージ参照先（1920＊1080）
   * @var unknown
   */
  // define('DEV_STORY_EDIT_IMAGE_BASE_1920', '/image/dev/android/1920x1080/');
  // define('DEV_STORY_EDIT_IMAGE_BASE_ORIGINAL', '/image/dev/android/original/');
  /**
   * 税率（無料）
   * @var unknown
   */
  define('TAX_RATE_TYPE_0', '0');
  /**
   * 税率（無料）
   * @var unknown
   */
  define('TAX_RATE_TYPE_5', '1');
  /**
   * 税率（無料）
   * @var unknown
   */
  define('TAX_RATE_TYPE_8', '2');
  /**
   * 税率（無料）
   * @var unknown
   */
  define('TAX_RATE_TYPE_10', '3');
  /**
   * qtv_video.title.site_flg:VM(docomo)
   * @var unknown
   */
  define('SITE_FLAG_VM_DOCOMO', '1');
  /**
   * qtv_video.title.site_flg:VM(au)
   * @var unknown
   */
  define('SITE_FLAG_VM_AU', '16');
  /**
   * qtv_video.title.site_flg:VM(sbm)
   * @var unknown
   */
  define('SITE_FLAG_VM_SBM', '8');
  /**
   * qtv_video.title.site_flg:QTVドーガ
   * @var unknown
   */
  define('SITE_FLAG_VM_DOGA', '2');
  /**
   * qtv_video.title.site_flg:てのひら動画
   * @var unknown
   */
  define('SITE_FLAG_VM_TENOHIRA', '64');
  /**
   * 字幕吹替タイプ　なし
   * @var unknown
   */
  define('SUBTITLE_DUB_TYPE_NONE', '0');
  /**
   * 字幕吹替タイプ　吹替
   * @var unknown
   */
  define('SUBTITLE_DUB_TYPE_DUB', '1');
  /**
   * 字幕吹替タイプ　字幕
   * @var unknown
   */
  define('SUBTITLE_DUB_TYPE_SUBTITLE', '2');
  /**
   * 字幕吹替タイプ　吹替・字幕付
   * @var unknown
   */
  define('SUBTITLE_DUB_TYPE_WITH_SUBTITLE', '3');
  /**
   * 字幕吹替タイプ　吹替・字幕両方
   * @var unknown
   */
  define('SUBTITLE_DUB_TYPE_BOTH', '4');
